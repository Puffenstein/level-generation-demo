## Summary

This is a demo project showing level generation and destruction as implemented by me in the Unity engine. The randomised level generation is based on the cave generation algorithms found in the official tutorials offered by Unity. I have built a destruction system on top of that  level generation which still requires some optimisation to be able to run on mobile devices. As this is a demo, it is a trimmed down version of a  larger scope ongoing project. 

## Instructions

This is a Unity 5.5 project which you can import into Unity and run it from there. Alternatively, there are OSX and Windows builds of the demo inside the 'builds' folder. 
The demo includes a tank that can be controlled by draggin the virtual joystick around the screen. There is also a shoot button (also mapped to SPACE, or your Unity jump key), which can be used to shoot projectiles which can destroy the walls. To generate a new randomised level, press 'S' or the corresponding GUI button in the top right corner of the screen.