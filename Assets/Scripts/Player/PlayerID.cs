﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[NetworkSettings (channel = 0, sendInterval = 1)]
public class PlayerID : MonoBehaviour
{
	public string playerName;
	public int score;

	private bool firstEnable = true;

	void SetName (string name)
	{
		playerName = name;
	}

	void OnDisable ()
	{
		ConnectedPlayers.RemovePlayer (this);
	}

	void OnEnable ()
	{
		if (firstEnable) {
			firstEnable = false;
			return;
		}
		ConnectedPlayers.AddPlayer (this);
	}
}
