﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ConnectedPlayers
{
	public static event System.Action<PlayerID> OnPlayerAdded;
	public static event System.Action<PlayerID> OnPlayerRemoved;

	static List<PlayerID> players = new List<PlayerID> ();

	public static void AddPlayer (PlayerID newPlayer)
	{
		players.Add (newPlayer);
		if (OnPlayerAdded != null) {
			OnPlayerAdded (newPlayer);
		}
	}

	public static void RemovePlayer (PlayerID player)
	{
		if (players.Contains (player)) {
			players.Remove (player);
			if (OnPlayerRemoved != null) {
				OnPlayerRemoved (player);
			}
		}
	}

	public static void Reset ()
	{
		if (OnPlayerRemoved != null) {
			foreach (var player in players) {
				OnPlayerRemoved (player);	
			}
		}
		
		players.Clear ();
	}
}
