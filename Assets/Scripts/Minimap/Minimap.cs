﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Minimap : MonoBehaviour, IPointerDownHandler
{
	public Image minimapImage;
	/** Size multiplier for when the minimap is small. */
	public float smallSize;
	/** Size multiplier for when the minimap is big. */
	public float bigSize;

	int width, height;
	float currentSizeMultiplier;

	public void SetMinimapTexture (Texture2D tex)
	{
		width = tex.width;
		height = tex.height;
		currentSizeMultiplier = smallSize;

		minimapImage.rectTransform.sizeDelta = new Vector2 (width * smallSize, height * smallSize);
		Sprite newSprite = Sprite.Create (tex, new Rect (0, 0, width, height), Vector2.zero);
		minimapImage.sprite = newSprite;
	}

	public void SwitchSize ()
	{
		StopAllCoroutines ();
		StartCoroutine (SmoothResize (currentSizeMultiplier == smallSize ? bigSize : smallSize));
	}

	IEnumerator SmoothResize (float newMultiplier)
	{
		while (currentSizeMultiplier != newMultiplier) {
			currentSizeMultiplier = Mathf.MoveTowards (currentSizeMultiplier, newMultiplier, Time.deltaTime * 4);
			minimapImage.rectTransform.sizeDelta = new Vector2 (width * currentSizeMultiplier, height * currentSizeMultiplier);
			yield return null;
		}
	}

	public void OnPointerDown (PointerEventData data) {
		SwitchSize (); 
	}
}
