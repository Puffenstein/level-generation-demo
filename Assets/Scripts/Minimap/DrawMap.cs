﻿using UnityEngine;
using System.Collections;

public class DrawMap : MonoBehaviour
{
	public Minimap minimap;
	/** Time between updates of the mini map. */
	public float updateTime;
	/** The level that is currently output by the mini map. */
	protected ProceduralLevel currentLevel;
	/** The tank which is displayed on the mini map. */
	protected Tank currentTank;

	protected Texture2D tex;

	int tankX, tankY;

	public void StartDrawing ()
	{
		currentLevel = LevelManager.Instance.currentLevel;
		currentTank = GameController.Instance.localPlayerTank;
		if (currentLevel == null || currentTank == null) {
			Invoke ("StartDrawing", 0.1f);
		} else {
			StartDrawing (currentLevel, currentTank);
		}
	}

	public void StartDrawing (ProceduralLevel level, Tank tank)
	{
		currentLevel = level;
		currentTank = tank;

		tex = new Texture2D (currentLevel.width, currentLevel.height);
		minimap.SetMinimapTexture (tex);

		MakeInitialMapDrawing ();

		currentLevel.OnLevelChanged += UpdateTile;

		InvokeRepeating ("UpdatePlayerPosition", updateTime, updateTime);
	}

	public void StopDrawing ()
	{
		CancelInvoke (); 
	}

	/**
	 * Draws the full map (walls and player) onto the minimap texture.
	 * */
	void MakeInitialMapDrawing ()
	{
		if (currentTank == null || currentLevel == null) {
			StopDrawing ();
			return;
		}

		for (int x = 0; x < currentLevel.width; x++) {
			for (int y = 0; y < currentLevel.height; y++) {
				tex.SetPixel (x, y, GetColorForTile (x, y));
			}
		}
	
		UpdatePlayerPosition ();

		tex.Apply ();
	}

	Color GetColorForTile (RandomLevelGenerator.TileCoord tileCoordinate)
	{
		return GetColorForTile (tileCoordinate.tileX, tileCoordinate.tileY);
	}

	Color GetColorForTile (int x, int y)
	{
		return GetColorForTileValue (currentLevel.GetCell (x, y));
	}

	Color GetColorForTileValue (int tileValue)
	{
		switch (tileValue) {
		case (int)RandomLevelGenerator.CellFillType.Empty:
			return Color.white;
		case (int)RandomLevelGenerator.CellFillType.OutsideWall:
			return Color.black;
		case (int)RandomLevelGenerator.CellFillType.Wall:
			return Color.gray;
		case (int)RandomLevelGenerator.CellFillType.SolidWall:
			return Color.black;
		default:
			throw new UnityException ("Unkown tile type in minimap."); 
		}
	}

	/**
	 * Clears the player position marker from the minimap and draws it in the current
	 * position of the player.
	 * */
	void UpdatePlayerPosition ()
	{
		// Clear old marker
		for (int x = tankX - 1; x <= tankX + 1; x++) {
			for (int y = tankY - 1; y <= tankY + 1; y++) {
				if (currentLevel.InRange (x, y) && (x == tankX || y == tankY)) {
					tex.SetPixel (x, y, GetColorForTile (x, y));
				}
			}
		}
		
		// Set new marker
		currentLevel.World2Map ((Vector2)currentTank.transform.position, out tankX, out tankY); // Update the tank position
		for (int x = tankX - 1; x <= tankX + 1; x++) {
			for (int y = tankY - 1; y <= tankY + 1; y++) {
				if (currentLevel.InRange (x, y) && (x == tankX || y == tankY)) {
					tex.SetPixel (x, y, Color.red);
				}
			}
		}
		tex.Apply ();
	}

	void UpdateTile (RandomLevelGenerator.TileCoord tileCoordinates)
	{
		tex.SetPixel (tileCoordinates.tileX, tileCoordinates.tileY, GetColorForTile (tileCoordinates)); 
		tex.Apply ();
	}
}
