﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class UsefulCoroutines {
	
	/**
	 * Executes the provided action after the specified amount of time.
	 */
	public static IEnumerator DelayAction (float delay, System.Action action, MonoBehaviour coroutineOwner) {
		if (coroutineOwner == null) {
			Debug.LogWarning ("Null coroutine owner. Delayed action will not execute.");
			return null;
		} else {
			IEnumerator coroutine = Delay (delay, action);
			coroutineOwner.StartCoroutine (coroutine);
			return coroutine;
		}
	}

	/**
	 * Executes the provided action after the specified amount of time.
	 * */
	public static IEnumerator DelayActionUnscaledTime (float delay, System.Action action, MonoBehaviour coroutineOwner)
	{
		if (coroutineOwner == null) {
			Debug.LogWarning ("Null coroutine owner. Delayed action will not execute.");
			return null;
		} else {
			IEnumerator coroutine = DelayUnscaled (delay, action);
			coroutineOwner.StartCoroutine (coroutine);
			return coroutine;
		}
	}

	public static IEnumerator DelayActionByAFrame (System.Action action, MonoBehaviour coroutineOwner) {
		if (coroutineOwner == null) {
			Debug.LogWarning ("Null coroutine owner. Delayed action will not execute.");
			return null;
		} else {
			IEnumerator coroutine = Delay (action);
			coroutineOwner.StartCoroutine (coroutine);
			return coroutine;
		}
	}



	static IEnumerator DelayUnscaled (float delay, System.Action action) {
		float startTime = Time.unscaledTime;
		while (Time.unscaledTime - startTime < delay) {
			yield return null;
		}

		if (action != null) {
			action ();
		}
	}

	static IEnumerator Delay (float delay, System.Action action) {
		yield return new WaitForSeconds (delay);
		if (action != null) {
			action ();
		}
	}

	static IEnumerator Delay (System.Action action) {
		yield return null;
		if (action != null) {
			action ();
		}
	}




	public static void DelayActionUntil (System.Action action, System.Func<bool> condition, MonoBehaviour coroutineOwner)
	{
		if (!coroutineOwner) {
			Debug.LogWarning ("Null coroutine owner. Delayed action will not execute.");
		} else {
			coroutineOwner.StartCoroutine (DelayUntil (action, condition));
		}
	}

	static IEnumerator DelayUntil (System.Action action, System.Func<bool> condition)
	{
		yield return new WaitUntil (condition);
		if (action != null) {
			action ();
		}
	}

	/**
	 * Takes a screenshot with a unique name based on the time of taking the screenshot.
	 * The SSRatio is used to supersample the screenshot, thus increase its resolution.
	 * */
	public static void CaptureScreenshot (int SSRatio) {
		System.DateTime time = System.DateTime.Now;
		string filename = string.Format ("{0}.{1}.{2}scrshot.png", time.Hour, time.Minute, time.Second);
		Application.CaptureScreenshot (filename, SSRatio);
	}

	#region String saving to streaming assets
	private static void Save (string s, string relativePath) {
		string path = Application.streamingAssetsPath + "/" + relativePath;

		File.WriteAllText (path, s);

//		using (FileStream outStream = File.OpenWrite (path)) {
//			using (StreamWriter writer = new StreamWriter (outStream)) {
//				writer.Write (s);
//			}
//		}
	}

	/**
	 * Saves a string in straming assets in the relative path inside of the streaming assets folder.
	 * The directory has to exist for the writing to be successful.
	 * */
	public static void SaveStringToStreamingAssets (string s, string relativePath) {
		if (s == null || relativePath == null || relativePath.Length == 0) {
			Debug.LogError ("Invalid arguments; cannot save to streaming assets.");
			return;
		}

		#if UNITY_ANDROID && !UNITY_EDITOR
		Debug.LogError ("Saving on android is not supported.");
		#else
		Save (s, relativePath);
		#endif
	}
	#endregion

	#region String loading from streaming assets

	private static string LoadFromAndroid (string relativePath) {
		string path = Application.streamingAssetsPath + "/" + relativePath;

		WWW file = new WWW(path);

		while (!file.isDone) {
			// Wait for the file to be loaded
			// This will block the game from executing, but that's alright since
			// we do not want the game to run before loading the level, do we?
		}

		using (MemoryStream mStream = new MemoryStream (file.bytes)) {
			using (StreamReader reader = new StreamReader (mStream)) {
				return reader.ReadToEnd ();
			}
		}
	}

	private static string Load (string relativePath) {
		string path = Application.streamingAssetsPath + "/" + relativePath;

		using (FileStream inStream = File.OpenRead (path)) {
			using (StreamReader reader = new StreamReader (inStream)) {
				return reader.ReadToEnd ();
			}
		}
	}

	/**
	 * Loads and returns the contents of a file as a string.
	 * The file is at relative path as specified, relative to the
	 * streaming assets directory.
	 * */
	public static string LoadStringFromStreamingAssets (string relativePath) {
		if (relativePath == null || relativePath.Length == 0) {
			Debug.LogError ("Invalid parameters in load from streaming assets; cannot load string.");
			return null;
		}

		#if UNITY_ANDROID && !UNITY_EDITOR
		return LoadFromAndroid (relativePath);
		#else
		return Load (relativePath);
		#endif
	}
	#endregion
}
