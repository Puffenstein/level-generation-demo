﻿using UnityEngine;
using System.Collections;

/**
 * This script enables all game objects in its list when it awakes.
 * */
public class GameObjectEnabler : MonoBehaviour
{
	[SerializeField] GameObject[] objectsToEnable;

	void Awake ()
	{
		foreach (var obj in objectsToEnable) {
			obj.SetActive (true);
		}
	}
}
