﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

/**
 * Similarly to UsefulCoroutines, this class containes useful methods that can be
 * used in many occasions, but these methods are not coroutines.
 * */
public static class UsefulMethods
{
	/**
	 * A method that returns a collection of all values of a given
	 * enum.
	 * */
	public static IEnumerable<T> GetEnumValues<T> ()
	{
		return System.Enum.GetValues (typeof(T)).Cast<T> ();
	}

	/**
	 * Converts a bytes array to an integer.
	 * */
	public static int Byte2Int (byte[] bytes)
	{
		return BitConverter.ToInt32 (bytes, 0);
	}

	/**
	 * Converts an integer to a byte array.
	 * */
	public static byte[] Int2Byte (int number)
	{
		return BitConverter.GetBytes (number);
	}
}
