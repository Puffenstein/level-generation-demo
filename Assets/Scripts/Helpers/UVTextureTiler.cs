﻿using UnityEngine;
using System.Collections;

/**
 * A script which can recalculate the UVs of a QUAD mesh
 * so that a texture applied to it tiles a set number of times
 * per unit length.
 * */
[ExecuteInEditMode]
public class UVTextureTiler : MonoBehaviour
{
	/** The length of the side of a single tile of texture. */
	[SerializeField] float tileSize = 1f;

	MeshFilter meshFilter;

	void Awake ()
	{
		meshFilter = GetComponent<MeshFilter> ();
		if (!meshFilter) {
			throw new MissingComponentException ("UVTextureTiler is missing a mesh filter component attached!");
		}
	}

	void Start ()
	{
		RecalculateUVs ();
	}

	/**
	 * Recalculates the UVs for the quad according to the
	 * quad's current size, and applies those UVs to the quad's mesh.
	 * */
	public void RecalculateUVs ()
	{
		float width = transform.localScale.x;
		float height = transform.localScale.y;

		width /= tileSize;
		height /= tileSize;

		Mesh m = meshFilter.sharedMesh;
		Vector2[] uvs = m.uv;
		uvs [0] = Vector2.zero;
		uvs [1] = new Vector2 (width, height);
		uvs [2] = new Vector2 (width, 0);
		uvs [3] = new Vector2 (0, height);
		m.uv = uvs;
	}

	#if UNITY_EDITOR
	void Update ()
	{
		RecalculateUVs ();
	}
	#endif
}
