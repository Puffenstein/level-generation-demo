﻿using UnityEngine;
using System.Collections;

public enum Language 
{
	English
}

public static class LanguageManager
{
	public static Language CurrentLanguage = Language.English;
}
