﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class StringResources
{
	public enum Tag {
		SearchingMatch, JoiningMatch,
		HostingMatch
	}

	#region English language strings
	readonly static Dictionary<Tag, string> englishStrings = new Dictionary<Tag, string> {
		{Tag.SearchingMatch, "Searching for games..."},
		{Tag.JoiningMatch, "Joining match..."},
		{Tag.HostingMatch, "Hosting a new game..."}
	};
	#endregion

	/**
	 * Returns the string resource for the specified tag and the currently used language.
	 * */
	public static string GetString (Tag tag)
	{
		return GetString (tag, LanguageManager.CurrentLanguage);
	}

	/**
	 * Returns the string resource for the specified tag and language.
	 * */
	public static string GetString (Tag tag, Language l)
	{
		return GetDictionary (l)[tag];
	}

	static Dictionary<Tag, string> GetDictionary (Language l)
	{
		switch (l) {
		case Language.English:
			return englishStrings;
		default:
			return null;
		}
	}
}
