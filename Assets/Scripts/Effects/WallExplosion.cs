﻿using UnityEngine;
using System.Collections;

public class WallExplosion : MonoBehaviour
{
	[SerializeField] ParticleSystem particles;
	[SerializeField] Animator smoke;

	public void Explode ()
	{
		particles.Play ();
		smoke.SetTrigger ("Explode");
	}

	public void ExplodeAt (Vector3 position)
	{
		transform.position = position;
		Explode (); 
	}
}
