﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EffectsManager : MonoBehaviour 
{
	public static EffectsManager Instance;
	[SerializeField] WallExplosion wallExplosionPrefab;

	Stack<WallExplosion> availableExplosions = new Stack<WallExplosion> ();

	void Awake ()
	{
		if (Instance != null) {
			Destroy (this); 
		} else {
			Instance = this;
		}
	}

	void OnDestroy ()
	{
		if (Instance == this) {
			Instance = null;
		}
	}

	public void WallExplosionAt (Vector3 position)
	{
		WallExplosion we = null;
		if (availableExplosions.Count > 0) {
			we = availableExplosions.Pop ();
		} else {
			we = Instantiate<WallExplosion> (wallExplosionPrefab);
		}
		we.ExplodeAt (position); 
		UsefulCoroutines.DelayAction (1.1f, () => availableExplosions.Push (we), this); 
	}
}
