﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

/**
 * The physics model for moving a tank.
 * */
[RequireComponent (typeof(Tank))]
[RequireComponent (typeof(Rigidbody2D))]
public class TankMovement : MonoBehaviour
{
	protected const float MAX_CORNERING_ANGLE = 125;

	/** The tank movement will accept joystick input as long as this flag is true. */
	bool inputEnabled = true;

	/** The direction into which the tank is pointing. */
	public float CurrentHeading {
		get {
			float heading = rbody.rotation;
			if (heading < 0) {
				heading += 360;
			}
			return heading;
		}
	}

	protected Tank tank;

	protected Rigidbody2D rbody;

	protected float currentSpeed;

	protected float targetSpeed;

	protected float targetHeading;

	private bool forwardCollision = false;

	private IEnumerator forwardCollisionReset = null;

	/** 
	 * Sawning seems to mess up with the Z coordinate, so I have to back it up and reapply it after spawn. 
	 * This can also be used for eventually allowing tanks to change their elevation, i.e. climb onto things. */
	protected float tankZ = -0.3f;

	void Awake ()
	{
		tank = GetComponent<Tank> ();
		rbody = GetComponent<Rigidbody2D> ();

		tank.OnTankDestroyed += (obj) => {
			Stop ();
			inputEnabled = false;
		};
		tank.OnTankReset += (obj) => {
			Stop ();
			inputEnabled = true;
		};
		targetHeading = CurrentHeading;
	}

	/**
	 * Moves the tank to the specified position in world space and 
	 * orients the tank so that it faces the specified heading.
	 * The heading is clamped between 0 and 360 degrees, where 0 is 
	 * facing north (up on the screen).
	 * */
	public void Init (Vector2 position, float heading)
	{
		// Apply the position to the tank.
		Vector3 tmp;
		tmp.x = position.x;
		tmp.y = position.y;
		tmp.z = tankZ;
		transform.position = tmp;
		// Apply the heading (rotation) to the tank.
		heading = Mathf.Clamp (heading, 0, 360);
		transform.localRotation = Quaternion.Euler (0, 0, heading);
		// Reset the current speed of the tank
		currentSpeed = 0;
	}

	void FixedUpdate ()
	{
		if (inputEnabled) {
			UpdateTargetVelocity ();
			Move (Time.deltaTime);
			// TODO Add level bounds checking
		}
	}

	/**
	 * Sets the target velocity according to the current mobile
	 * joystic input values.
	 * */
	void UpdateTargetVelocity ()
	{
		Vector2 joysticValue = new Vector2 (CrossPlatformInputManager.GetAxis ("Horizontal"), CrossPlatformInputManager.GetAxis ("Vertical"));
		// Assuming the joystic value is capped to a magnitude of 1, to get the target velocity 
		// it's only needed to multiply the joystic value by the top speed
		targetSpeed = joysticValue.magnitude * tank.TopSpeed;
		targetHeading = Quaternion.FromToRotation (Vector2.up, joysticValue).eulerAngles.z;
	}

	/**
	 * Modifies the current velocity to try and match the target velocity
	 * and then moves the tank according to the current velocity.
	 * */
	void Move (float deltaTime)
	{
		if (currentSpeed > 2.5f && rbody.velocity.sqrMagnitude < 0.01f) {
			// Usually the actual velocity of the rigid body is ignore, but if the tank bumps into something from high speed
			// then the current speed will not correspond to the rigid body's velocity anymore, so it better be updated manually
			currentSpeed = 0;
		}
		if (targetSpeed == 0) {
			// The joystick is released: keep current heading and stop
			currentSpeed = Mathf.MoveTowards (currentSpeed, targetSpeed, deltaTime * tank.MaxDeceleration);
		} else {
			float heading = CurrentHeading;
			// Find the smalles angle between the two headings
			float angleDiff = Mathf.Abs (heading - targetHeading);
			angleDiff = Mathf.Min (360f - angleDiff, angleDiff);

			// Tho joystick is being controlled: turn towards heading and accelerate
			if (angleDiff < MAX_CORNERING_ANGLE) {
				// If the turn is not too sharp, start accelerating towards target speed
				currentSpeed = Mathf.MoveTowards (
					currentSpeed, 
					Mathf.Max (tank.MinTurningSpeed, targetSpeed * (1 - angleDiff / MAX_CORNERING_ANGLE)) * (forwardCollision ? -1 : 1), 
					deltaTime * GetAcceleration (currentSpeed, tank.TopSpeed, tank.MaxAcceleration)
				);
			} else {
				// If the turn is too sharp, brake for the turn
				currentSpeed = Mathf.MoveTowards (currentSpeed, 
					forwardCollision ? -tank.MinTurningSpeed : tank.MinTurningSpeed, 
					deltaTime * tank.MaxDeceleration);
			}
			// Set the heading
			heading = Mathf.MoveTowardsAngle (heading, targetHeading, deltaTime * tank.TurnRate);
//			transform.localRotation = Quaternion.Euler (0, 0, heading);
			rbody.rotation = heading;
		}

		// Finally, move the tank
		rbody.position = rbody.position + (Vector2)(transform.up * currentSpeed * deltaTime);
		rbody.velocity = transform.up * currentSpeed;
//		rbody.AddForce (transform.up * targetSpeed * 500, ForceMode2D.Force);
	}

	void OnCollisionStay2D (Collision2D col)
	{
		Vector2 toCollision = col.contacts [0].point - rbody.position;
		Vector2 forward = transform.up;
		float dot = Vector2.Dot (toCollision.normalized, forward.normalized);
		if (dot > 0.85f) {
			forwardCollision = true;
			if (forwardCollisionReset != null) {
				StopCoroutine (forwardCollisionReset);
			}
			forwardCollisionReset = UsefulCoroutines.DelayAction (.5f, () => {
				forwardCollision = false;
				forwardCollisionReset = null;
			}, this);
		}
	}

	/**
	 * Immediately stops the tank in its tracks.
	 * */
	void Stop ()
	{
		currentSpeed = 0;
		targetSpeed = 0;
	}

	/**
	 * This method does inverse linear interpolation where:
	 * if current speed = 0, returned acceleration is max acceleration
	 * if current speed = top speed, returned acceleration is 0
	 * */
	static float GetAcceleration (float currentSpeed, float topSpeed, float maxAcceleration)
	{
		return Mathf.Lerp (maxAcceleration, 0, currentSpeed / topSpeed - 0.5f);
	}
}