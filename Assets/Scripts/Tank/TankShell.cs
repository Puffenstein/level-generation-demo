﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
public class TankShell : MonoBehaviour
{
	public const float PROJECTILE_SPEED = 20;
	public const float SHELL_LIFETIME = 3;

	public int explosionRadius = 1;

	Rigidbody2D rbody;

	Collider2D col;

	Tank fromTank;

	void Awake ()
	{
		rbody = GetComponent<Rigidbody2D> ();
		col = GetComponent<BoxCollider2D> ();
	}

	void Start ()
	{
		// Just make sure that the projectile doesn't collide with the tank that shot it
		foreach (Tank tank in GameObject.FindObjectsOfType<Tank> ()) {
			if (fromTank == tank) {
				Physics2D.IgnoreCollision (col, tank.TankCollider);
			}
		}
	}

	public void ShootForward (Tank fromTank)
	{
		ShootInDirection (fromTank, fromTank.transform.up);
	}

	public void ShootInDirection (Tank fromTank, Vector2 direction)
	{
		this.fromTank = fromTank;

		rbody.transform.rotation = Quaternion.FromToRotation (Vector3.up, direction);
		transform.position = fromTank.transform.position + 0.5f * transform.up;
		rbody.velocity = transform.up * PROJECTILE_SPEED;

		Destroy (gameObject, SHELL_LIFETIME);
	}

	void DestroyShell ()
	{
		Destroy (gameObject);
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.CompareTag ("Tank")) {
			// The object that is hit should be a tank
			Tank tank = col.collider.gameObject.GetComponent<Tank> ();
			if (tank != null) {
				if (tank != fromTank) {
					// The shell hit another tank
					tank.RpcDestroyTank ();
				}
			}
		} else if (col.gameObject.CompareTag ("Destructible")) {
			LevelManager.Instance.RpcExplodeWall (col.contacts[0].point, explosionRadius);
		}

		// The shell hit something, destroy the shell
		DestroyShell ();
	}
}
