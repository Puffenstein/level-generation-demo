﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;

/**
 * Representation of a tank with all of its specifications, such
 * as top speed, acceleration and turn rate.
 * */
[RequireComponent (typeof(TankMovement))]
public class Tank : MonoBehaviour
{
	/** Invoked when the DestroyTank () method is called on this tank. */
	public event System.Action<Tank> OnTankDestroyed;
	/** Invoked when the ResetTank (...) method is called on this tank. */
	public event System.Action<Tank> OnTankReset;


	/** The tank movement will accept joystick input as long as this flag is true. */
	//	public bool isLocalPlayer = true;

	public Collider2D TankCollider { get; protected set; }

	/** The maximum speed of the tank in a straight line (in units/second). */
	[SerializeField] protected float topSpeed;

	/** The max acceleration of the tank (in units/second^2). */
	[SerializeField] protected float maxAcceleration;

	/** The max deceleration of the tank (in units/second^2). */
	[SerializeField] protected float maxDeceleration;

	/** This is the speed with which the tank will be moving when it is doing a sharp turn. */
	[SerializeField] protected float minTurningSpeed;

	/** The rate at which the tank is turning (in degrees/second). */
	[SerializeField] protected float turnRate;

	/** The time in seconds that it takes to reload the tank cannon. */
	[SerializeField] protected float reloadTime;

	[SerializeField] protected Animator explosion;

	/** A prefab which will be instantiated as the tank shoots a projectile. */ 
	[SerializeField] protected TankShell shellPrefab;

	protected Renderer tankRenderer;

	protected TrailRenderer[] trails;

	protected TankMovement tankMovement;

	private float lastShotTime;

	private bool firstStart = true;

	void Awake ()
	{
		tankMovement = GetComponent<TankMovement> ();
		tankRenderer = GetComponent<SpriteRenderer> ();
		TankCollider = GetComponent<BoxCollider2D> ();
		trails = GetComponentsInChildren<TrailRenderer> ();
	}

	void Start ()
	{
		// Make the camera follow the local player
		CameraController.Instance.SetTank (this);
		if (firstStart) {
			ResetTank ();
			firstStart = false;
		}
	}

	#region Properties used for access to the tank specifications

	/** The maximum speed of the tank in a straight line (in units/second). */
	public float TopSpeed {
		get {
			return topSpeed;
		}
	}

	/** The max acceleration of the tank (in units/second^2). */
	public float MaxAcceleration {
		get {
			return maxAcceleration;
		}
	}

	/** The max deceleration of the tank (in units/second^2). */
	public float MaxDeceleration {
		get {
			return maxDeceleration;
		}
	}

	/** The rate at which the tank is turning (in degrees/second). */
	public float TurnRate {
		get {
			return turnRate;
		}
	}

	/** The time in seconds that it takes to reload the tank cannon. */
	public float ReloadTime {
		get {
			return reloadTime;
		}
	}

	public float MinTurningSpeed {
		get {
			return minTurningSpeed;
		}
	}

	#endregion

	void CmdResetTank ()
	{
		RpcSetVisible (true);
		RpcResetTank ();
	}

	/**
	 * Resets the tank in a random spot in the currently loaded level.
	 * NOTE: method left over from networked version of the game
	 * */
	void RpcResetTank ()
	{
		ResetTank ();
	}

	/**
	 * Spawns the tank in a random empty spot of the map, if there is currently a map, and also pointing a random direction.
	 * */
	public void ResetTank ()
	{
		int x, y;
		ProceduralLevel lvl = LevelManager.Instance.currentLevel;
		if (lvl == null) {
			return;
		}
		lvl.GetRandomEmptyCell (out x, out y); 
		Vector2 pos = lvl.Map2World (x, y);
		ResetTank (pos, Random.Range (0f, 360f));
	}

	/**
	 * Spawns the tank in a random spot in the spawn area provided and also pointing a random direction.
	 * */
//	public void ResetTank (Rect spawnArea)
//	{
//		Vector2 position;
//		position.x = Mathf.Lerp (spawnArea.xMin, spawnArea.xMax, Random.value);
//		position.y = Mathf.Lerp (spawnArea.yMin, spawnArea.yMax, Random.value);
//		ResetTank (position, Random.Range (0f, 360f));
//	}

	/**
	 * Resets the tank by making it visible and positioning it
	 * at the specified position.
	 * */
	public void ResetTank (Vector2 position, float heading)
	{
		tankMovement.Init (position, heading);
		foreach (var trail in trails) {
			trail.enabled = true;
		}
		if (OnTankReset != null) {
			OnTankReset (this);
		}
	}

	/** This is a call from the server which triggers this particular tank's destruction on all clients. */
	public void RpcDestroyTank ()
	{
		DestroyTank ();
	}

	/**
	 * De stroys the tank by destroying its game object and playing an
	 * explosion animation.
	 * */
	public void DestroyTank (bool alsoDestroyObject = false)
	{
		explosion.SetTrigger ("Explode");
		// Disable trails
		foreach (var trail in trails) {
			trail.enabled = true;
		}
		// Make the tank invisible when the explosion's fireball has covered the entire tank
		// TODO: Instead of hiding the tank, replace its sprite by debris sprite
		UsefulCoroutines.DelayAction (.2f, () => SetVisible (false), this);
		if (OnTankDestroyed != null) {
			OnTankDestroyed (this);
		}

		UsefulCoroutines.DelayAction (2, () => CmdResetTank (), this);

		if (alsoDestroyObject) {
			// Destroy the game object after the explosion animation has finished playing
			Destroy (gameObject, 2);	
		}
	}

	public void TakeShot ()
	{
		TankShell shell = Instantiate (shellPrefab);
		shell.ShootForward (this);
	}

	public bool TryShoot ()
	{
		if (Time.time - lastShotTime < ReloadTime) {
			// The tank is reloading
			return false;
		} else {
			// The tank is ready to shoot
			TakeShot ();	// Call upon the server to execute a shoot action and spawn a new bullet
			lastShotTime = Time.time;

			return true;
		}
	}

	protected void RpcSetVisible (bool isVisible)
	{
		SetVisible (isVisible);
	}

	protected void SetVisible (bool isVisible)
	{
		tankRenderer.enabled = isVisible;
		TankCollider.enabled = isVisible;
	}

//	public override void OnStartLocalPlayer ()
//	{
//		// Make the camera follow the local player
//		CameraController.Instance.SetTank (this);
//		GameController.Instance.SetTank (this);
//		if (firstStart) {
//			ResetTank ();
//			firstStart = false;
//		}
//	}

	void Update ()
	{
		// TEST DEBUG SUTFF
		if ((Input.GetKey (KeyCode.Space) || CrossPlatformInputManager.GetButton ("Jump"))) {
			TryShoot ();
		}
	}
}
