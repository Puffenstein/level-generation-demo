﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(ParticleSystem))]
public class DestructibleBlock : MonoBehaviour, Destructible
{
	ParticleSystem ps;
	Collider2D col;
	Renderer rend;

	void Awake ()
	{
		ps = GetComponent<ParticleSystem> ();
		col = GetComponent<Collider2D> ();
		rend = GetComponent<Renderer> ();
	}

	#region Destructible implementation

	void Destructible.Destroy ()
	{
		rend.enabled = false;
		col.enabled = false;
		ps.Play ();
	}

	void Destructible.Reset ()
	{
		rend.enabled = true;
		col.enabled = false;
		ps.Stop ();
	}

	bool Destructible.IsDestroyed ()
	{
		return !rend.enabled;
	}

	bool Destructible.Equals (Destructible d)
	{
		if (d.GetType () != typeof(DestructibleBlock)) {
			return false;
		}
		return this == d as DestructibleBlock;
	}

	#endregion
}
