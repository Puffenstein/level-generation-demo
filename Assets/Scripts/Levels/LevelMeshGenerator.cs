﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Copyright 2016, Ivaylo Parvanov, All rights reserved.
 * Implementation of Marching Square algorithm for generating a mesh out of the
 * randomly generated level map.
 * */
public class LevelMeshGenerator : MonoBehaviour
{
	#region Marching squares helper classes

	public class SquareGrid
	{
		public Square [,] squares;
		public ControlNode[,] controlNodes;

		public SquareGrid (int [,] levelMap, float squareSize = 1f)
		{
			int nodeCountX = levelMap.GetLength (0);
			int nodeCountY = levelMap.GetLength (1);

			float mapWidth = nodeCountX * squareSize;
			float mapHeight = nodeCountY * squareSize;

			controlNodes = new ControlNode[nodeCountX, nodeCountY];

			for (int x = 0; x < nodeCountX; x++) {
				for (int y = 0; y < nodeCountY; y++) {
					Vector3 position = new Vector3 (
						-mapWidth / 2 + x * squareSize + squareSize / 2, 
						-mapHeight / 2 + y * squareSize + squareSize / 2, 
						0);
					controlNodes[x, y] = new ControlNode (position, levelMap [x, y], squareSize);
				}
			}

			squares = new Square[nodeCountX - 1, nodeCountY - 1];
			for (int x = 0; x < nodeCountX - 1; x++) {
				for (int y = 0; y < nodeCountY - 1; y++) {
					squares [x, y] = new Square (
						controlNodes [x, y + 1], 
						controlNodes [x + 1, y + 1], 
						controlNodes [x + 1, y], 
						controlNodes [x, y]);
				}
			}
		}

		/**
		 * Updates the squares in the square grid to match the updated map without
		 * recreating the whole grid.
		 * */
		public void UpdateSquareGrid (int[,] updatedMap)
		{
			for (int x = 0; x < updatedMap.GetLength (0); x++) {
				for (int y = 0; y < updatedMap.GetLength (1); y++) {
					controlNodes [x, y].wallType = updatedMap [x, y];
//					controlNodes [x, y].ResetVertexIndex ();
				}
			}

			for (int x = 0; x < squares.GetLength (0); x++) {
				for (int y = 0; y < squares.GetLength (1); y++) {
					squares [x, y].UpdateConfiguration ();
				}
			}
		}

		bool InRange (int x, int y)
		{
			return x >= 0 && x < squares.GetLength (0) && y >= 0 && y < squares.GetLength (1);
		}
	}

	public class Square
	{
		public ControlNode nw, ne, se, sw;
		public Node n, e, s, w;
		/** One of 16 possible configurations of the 4 control nodes. */
		public int configuration;

		public bool onEdge {
			get;
			private set;
		}

		public Square (ControlNode northWest, ControlNode northEast, ControlNode southEast, ControlNode southWest)
		{
			nw = northWest;
			ne = northEast;
			se = southEast;
			sw = southWest;

			// Assign the midpoints keeping in mind that every control node owns the midpoints north and east of it.
			n = nw.east;
			e = se.north;
			s = sw.east;
			w = sw.north;

			UpdateConfiguration ();

			onEdge = nw.onEdge || ne.onEdge || se.onEdge || sw.onEdge;
		}

		public void UpdateConfiguration ()
		{
			configuration = 0;
			if (RandomLevelGenerator.IsWall (nw.wallType)) {
				configuration += 8;
			}
			if (RandomLevelGenerator.IsWall (ne.wallType)) {
				configuration += 4;
			}
			if (RandomLevelGenerator.IsWall (se.wallType)) {
				configuration += 2;
			}
			if (RandomLevelGenerator.IsWall (sw.wallType)) {
				configuration += 1;
			}
		}

		public Node[] GetNodes ()
		{
			return new Node[] { nw, ne, se, sw, n, e, s, w };
		}
	}

	public class Node
	{
		public Vector3 position;
		public int vertexIndex = -1;

		public Node (Vector3 pos)
		{
			position = pos;
		}
	}

	public class ControlNode : Node
	{
		public int wallType;
		public Node north, east;
		/** Set this to true if this node is on the outside edge of the 2D topographal level layout. */
		public bool onEdge;

		public ControlNode (Vector3 pos, int wallType, float squareSize) : base (pos)
		{
			this.wallType = wallType;
			this.north = new Node (pos + Vector3.up * squareSize / 2f);
			this.east = new Node (pos + Vector3.right * squareSize / 2f);

			onEdge = wallType == (int)RandomLevelGenerator.CellFillType.SolidWall;
		}

		public void ResetVertexIndex ()
		{
			vertexIndex = -1;
			north.vertexIndex = -1;
			east.vertexIndex = -1;
		}
	}

	#endregion

	#region Walls generation helper classes

	struct Triangle 
	{
		/** These are the indeces of the 3 vertices that form the triangle. */
		int[] vertices;

		public Triangle (int a, int b, int c)
		{
			vertices = new int[3];
			vertices[0] = a;
			vertices[1] = b;
			vertices[2] = c;
		}

		public int this [int i]
		{
			get {
				return vertices [i];
			}
		}

		public bool Contains (int vertexIndex)
		{
			return vertexIndex == vertices[0] ||
			vertexIndex == vertices[1] ||
			vertexIndex == vertices[2];
		}

		public override bool Equals (object obj)
		{
			Triangle t = (Triangle)obj;

			return vertices [0] == t [0] && vertices [1] == t [1] && vertices [2] == t [2];
		}

		public override int GetHashCode ()
		{
			return vertices [0] + vertices[1] * vertices[1] + vertices[2] * vertices[2];
		}
	}

	/**
	 * Struct describing sequence of verteces belonging to an outline.
	 * An outline is a continuous and looped edge of the 2D topographic level map layout
	 * to which a wall can be attached in the 3rd dimension.
	 * */
	class Outline : IEnumerable
	{
		/** The vertices of this outline. */
		public LinkedList<int> outlineVertices;
		public Rect bounds;

		/**
		 * Returns the index of the first point of the outline.
		 * */
		public int First
		{
			get {
				return outlineVertices.First.Value;
			}
		}

		/**
		 * Returns the index of the last point of the outline.
		 * */
		public int Last
		{
			get {
				return outlineVertices.Last.Value;
			}
		}

		public int Count
		{
			get {
				return outlineVertices.Count;
			}
		}

		/** If the edge is on the outside perimeter of the map, set this to true, 
		 * because outside edges have to be generated differently in order for their normals to be correct. */
		public bool IsOuterEdge;
		
		public Outline (bool isOuterEdge)
		{
			outlineVertices = new LinkedList<int> ();
			bounds = new Rect ();
			bounds.min = new Vector2 (float.MaxValue, float.MaxValue);
			bounds.max = new Vector2 (float.MinValue, float.MinValue);
			this.IsOuterEdge = isOuterEdge;
		}

		public void AddLast (int vertexIndex, Vector2 worldLocation)
		{
			outlineVertices.AddLast (vertexIndex);
			UpdateBounds (worldLocation); 
		}
		public void AddFirst (int vertexIndex, Vector2 worldLocation)
		{
			outlineVertices.AddFirst (vertexIndex);
			UpdateBounds (worldLocation); 
		}

		private void UpdateBounds (Vector2 worldLocation)
		{
			// Update the outline bounds
			if (worldLocation.x < bounds.xMin) {
				bounds.xMin = worldLocation.x;
			} 
			if (worldLocation.x > bounds.xMax) {
				bounds.xMax = worldLocation.x;
			}

			if (worldLocation.y < bounds.yMin) {
				bounds.yMin = worldLocation.y;
			} 
			if (worldLocation.y > bounds.yMax) {
				bounds.yMax = worldLocation.y;
			}			
		}

		public IEnumerator<int> GetEnumerator ()
		{
			return outlineVertices.GetEnumerator ();
		}

		public void SetOutsideEdge (bool b)
		{
			this.IsOuterEdge = b;
		}

		#region IEnumerable implementation

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return GetEnumerator ();
		}

		#endregion
	}

	#endregion

	#region 2D level topology generation
	const int MAX_OUTLINE_LENGTH = 60;
	public MeshFilter levelMeshFilter;
	/** Grid of 'marching square' algorithm squares. 
	 * Every square has a Node from the marching squares in each of its corners. */
	public SquareGrid squareGrid;
	/** All vertices of the generated 2D level topology mesh. */
	List<Vector3> vertices;
	/** Triangles of the generated 2D level topology mesh. */
	List<int> triangles;
	#endregion

	#region 3D walls generation
	public MeshFilter wallsMeshFilter;
	/** All generated outlines, where walls will be attached, will be stored here. */
	List<Outline> outlines = new List<Outline> ();
	/** Optimisation: do not check vertices twice when calculating outlines, and also do not check vertices that are not on the edge of the polygon. */
	List<bool> checkedVertices;
	/** Maps vertex index to triangles that  that vertex belongs to. */
	Dictionary<int, List<Triangle>> trianglesDictionary = new Dictionary<int, List<Triangle>> ();
	#endregion

	List<EdgeCollider2D> currentColliders = new List<EdgeCollider2D> ();

	float squareSize;

	RandomLevelGenerator rlg;

	public void GenerateMesh (int[,] levelMap, RandomLevelGenerator randLevelGen, float squareSize = 1f)
	{
		this.rlg = randLevelGen;

		this.squareSize = squareSize;
		outlines.Clear ();
		trianglesDictionary.Clear (); 

		squareGrid = new SquareGrid (levelMap, squareSize);

		vertices = new List<Vector3> ();
		triangles = new List<int> ();

		// Create the 2D mesh for the map topology layout
		for (int x = 0; x < squareGrid.squares.GetLength (0); x++) {
			for (int y = 0; y < squareGrid.squares.GetLength (1); y++) {
				TriangulateSquare (squareGrid.squares [x, y]);
			}
		}

		// Now that the vertices have been created, I can allocate an array to keep track of which 
		// vertices have been visited by the outline discovery algorithm
		checkedVertices = new List<bool> (vertices.Count);
		for (int i = 0; i < vertices.Count; i++) {
			checkedVertices.Add (false);
		}

		Mesh mesh = new Mesh ();
		levelMeshFilter.mesh = mesh;
		mesh.vertices = vertices.ToArray ();
		mesh.triangles = triangles.ToArray ();
		mesh.RecalculateNormals ();

		UpdateLevelUVs (levelMap, mesh);

		CreateWallMesh ();

		Generate2DColliders ();
	}

	/**
	 * Updates the last generated level and map meshes only in the region between (fromX, fromY) to (toX, toY).
	 * */
	public void PartialMeshUpdate (int[,] updatedLevelMap, int fromX, int fromY, int toX, int toY)
	{
		squareGrid.UpdateSquareGrid (updatedLevelMap);

		int squaresWidth = squareGrid.squares.GetLength (0);
		int squaresHeight = squareGrid.squares.GetLength (1);

		ExpandAndClamp (ref fromX, ref fromY, ref toX, ref toY, squaresWidth, squaresHeight);

		// Delete all trianlges belonging to the updates squares
		for (int x = fromX; x <= toX; x++) {
			for (int y = fromY; y <= toY; y++) {
				DeleteTrianglesForSquare (squareGrid.squares [x, y]); 
			}
		}

		// Triangulate the squares in the update range
		for (int x = fromX; x <= toX; x++) {
			for (int y = fromY; y <= toY; y++) {
				TriangulateSquare (squareGrid.squares [x, y]);
			}
		}

		// Now that the vertices have been created, I can allocate an array to keep track of which 
		// vertices have been visited by the outline discovery algorithm
		int diff = vertices.Count - checkedVertices.Count;
		for (int i = 0; i < diff; i++) {
			checkedVertices.Add (false);
		}

		// Update the mesh
		Mesh mesh = levelMeshFilter.mesh;
//		levelMeshFilter.mesh = mesh;
		mesh.vertices = vertices.ToArray ();
		mesh.triangles = triangles.ToArray ();
		mesh.RecalculateNormals ();

		UpdateLevelUVs (updatedLevelMap, mesh);

		PartialUpdateWallMesh (fromX, fromY, toX, toY);

		Update2DColliders ();
	}

	/**
	 * Helper method used in the partial mesh update procedure in order to expand the area of squares
	 * to be updated by one, and also clamp that area to the limits of the square grid.
	 * */
	void ExpandAndClamp (ref int fromX, ref int fromY, ref int toX, ref int toY, int squaresWidth, int squaresHeight)
	{
		fromX--;
		fromY--;
		toX++;
		toY++;

		fromX = Mathf.Clamp (fromX, 0, squaresWidth - 1);
		toX = Mathf.Clamp (toX, 0, squaresWidth - 1);
		fromY = Mathf.Clamp (fromY, 0, squaresHeight - 1);
		toY = Mathf.Clamp (toY, 0, squaresHeight - 1);
	}

	void UpdateLevelUVs (int[,] levelMap, Mesh mesh)
	{
		Vector2[] uvs = new Vector2[vertices.Count];
		int width = levelMap.GetLength (0);
		int height = levelMap.GetLength (1);
		for (int i =0; i < vertices.Count; i ++) {
			float percentX = Mathf.InverseLerp(-width/2*squareSize, width/2*squareSize, vertices[i].x);
			float percentY = Mathf.InverseLerp(-height/2*squareSize, height/2*squareSize, vertices[i].y);
			uvs[i] = new Vector2(percentX * width, percentY * height);
		}
		mesh.uv = uvs;
	}

	void TriangulateSquare (Square sq)
	{
		switch (sq.configuration) {
		case 0:
			break;
			// 1 control point cases
		case 1:
			MeshFromPoints (sq.w, sq.s, sq.sw);
			break;
		case 2:
			MeshFromPoints (sq.se, sq.s, sq.e);
			break;
		case 4:
			MeshFromPoints (sq.ne, sq.e, sq.n);
			break;
		case 8:
			MeshFromPoints (sq.nw, sq.n, sq.w);
			break;
			// 2 control point cases
		case 3:
			MeshFromPoints (sq.e, sq.se, sq.sw, sq.w);
			break;
		case 6:
			MeshFromPoints (sq.n, sq.ne, sq.se, sq.s);
			break;
		case 9:
			MeshFromPoints (sq.nw, sq.n, sq.s, sq.sw);
			break;
		case 12:
			MeshFromPoints (sq.nw, sq.ne, sq.e, sq.w);
			break;
			// 2 control points across
		case 5:
			MeshFromPoints (sq.n, sq.ne, sq.e, sq.s, sq.sw, sq.w);
			break;
		case 10:
			MeshFromPoints (sq.nw, sq.n, sq.e, sq.se, sq.s, sq.w);
			break;
			// 3 control point cases
		case 7:
			MeshFromPoints (sq.n, sq.ne, sq.se, sq.sw, sq.w);
			break;
		case 11:
			MeshFromPoints (sq.nw, sq.n, sq.e, sq.se, sq.sw);
			break;
		case 13:
			MeshFromPoints (sq.nw, sq.ne, sq.e, sq.s, sq.sw);
			break;
		case 14:
			MeshFromPoints (sq.nw, sq.ne, sq.se, sq.s, sq.w);
			break;
			// 4 control points case
		case 15:
			MeshFromPoints (sq.nw, sq.ne, sq.se, sq.sw);				
			break;
		default:
			throw new UnityException ("Unknown mesh configuration in level mesh generator.");
		}
	}

	void MeshFromPoints (params Node[] points)
	{
		// Add vertices to list
		AssignVertices (points);
		// Make triangles
		if (points.Length >= 3) {
			MakeTriangle (points [0], points [1], points [2]);
		}
		if (points.Length >= 4) {
			MakeTriangle (points [0], points [2], points [3]);
		}
		if (points.Length >= 5) {
			MakeTriangle (points [0], points [3], points [4]);
		}
		if (points.Length >= 6) {
			MakeTriangle (points [0], points [4], points [5]);
		}
	}

	void AssignVertices (Node[] points)
	{
		foreach (var point in points) {
			if (point.vertexIndex == -1) {
				point.vertexIndex = vertices.Count;
				vertices.Add (point.position);
			}
		}
	}

	void MakeTriangle (Node a, Node b, Node c)
	{
		triangles.Add (a.vertexIndex);
		triangles.Add (b.vertexIndex);
		triangles.Add (c.vertexIndex);

		Triangle t = new Triangle (a.vertexIndex, b.vertexIndex, c.vertexIndex);
		AddTriangleToDictionary (t[0], t);
		AddTriangleToDictionary (t[1], t);
		AddTriangleToDictionary (t[2], t);
	}

	#region Walls creation

	void CreateWallMesh (bool reuseMesh = false)
	{
		CalculateMeshOutlines ();

		List<Vector3> wallVertices = new List<Vector3> ();
		List<int> wallTriangles = new List<int> ();

		float wallHeight = 5;

		foreach (var outline in outlines) {
			LinkedListNode<int> currentNode = outline.outlineVertices.First;
			while (currentNode.Next != null) {
				int startIndex = wallVertices.Count;
				wallVertices.Add (vertices[currentNode.Value]);	// left vertex
				wallVertices.Add (vertices[currentNode.Next.Value]);	// right vertex
				wallVertices.Add (vertices[currentNode.Value] - Vector3.back * wallHeight);	// bottomleft vertex
				wallVertices.Add (vertices[currentNode.Next.Value] - Vector3.back * wallHeight);	// bottomright vertex

				// This order ensures that normals face the right way for walls that 
				// are on the inside of the map
				wallTriangles.Add (startIndex + 0);
				wallTriangles.Add (startIndex + 3);
				wallTriangles.Add (startIndex + 1);
				wallTriangles.Add (startIndex + 0);
				wallTriangles.Add (startIndex + 2);
				wallTriangles.Add (startIndex + 3); 

				currentNode = currentNode.Next;
			}
		}
		Mesh wallMesh = null;
		if (reuseMesh) {
			wallMesh = wallsMeshFilter.mesh;
		} else {
			wallMesh = new Mesh ();
		}

		wallMesh.vertices = wallVertices.ToArray ();
		wallMesh.triangles = wallTriangles.ToArray ();
		wallsMeshFilter.mesh = wallMesh;
		wallMesh.RecalculateNormals ();
	}

	void PartialUpdateWallMesh (int fromX, int fromY, int toX, int toY)
	{
		// Optimization 1: determine which outlines need to be updated and update only them
		Vector2 min = (Vector2)rlg.Coord2WorldPoint (new RandomLevelGenerator.TileCoord (fromX, fromY));
		Vector2 max = (Vector2)rlg.Coord2WorldPoint (new RandomLevelGenerator.TileCoord (toX, toY));

		Rect updateRect = new Rect (min, max - min);

		List<Outline> outlinesForUpdate = new List<Outline> ();
		foreach (var outline in outlines) {
			if (outline.bounds.Overlaps (updateRect)) {
				outlinesForUpdate.Add (outline);
			}
		}
		foreach (var outline in outlinesForUpdate) {
			// Mark the vertices of the outlines that need to be updated as not-checked, and then run the outline finding algorithm on them
			// Also, these outlines will need to be removed from the outlines list
			foreach (var vertexIndex in outline) {
				checkedVertices [vertexIndex] = false;
			}
			outlines.Remove (outline); 
		}

		CreateWallMesh ();
	}

	int GetConnectedOutlineVertex (int vertexIndex)
	{
		List<Triangle> trianglesForVertex = trianglesDictionary [vertexIndex];
		foreach (var triangle in trianglesForVertex) {
			for (int i = 0; i < 3; i++) {
				int vertexB = triangle [i];

				if (!checkedVertices[vertexB] && IsOutlineEdge (vertexIndex, vertexB)) {
					return vertexB;
				}
			}
		}

		return -1;
	}

	/**
	 * Returns a connected outline edge vertex clockwise from the current one, or counter clockwise
	 * from the current one depending on the value of the clockwise parameter.
	 * */
	int GetConnectedVertexInDirection (int vertexIndex, bool clockwise)
	{
		foreach (var triangle in trianglesDictionary [vertexIndex]) {
			int currentIndexInTriag = 0;
			for (int i = 0; i < 3; i++) {
				if (vertexIndex == triangle [i]) {
					currentIndexInTriag = i;
				}
			}
			for (int i = 0; i < 3; i++) {
				if (i == currentIndexInTriag) {
					continue;
				}
				int vertexB = triangle [i];
				// Find out which of the three points in the triangle is the one left after removing vertex B and the current vertex
				int remainingIndexInTriag = 3 - i - currentIndexInTriag;

				if (clockwise) {
					if (!checkedVertices[vertexB] && IsOutlineEdge (vertexIndex, vertexB) && IsClockwise (vertexIndex, vertexB, triangle[remainingIndexInTriag])) {
						return vertexB;
					}					
				} else {
					if (!checkedVertices[vertexB] && IsOutlineEdge (vertexIndex, vertexB) && !IsClockwise (vertexIndex, vertexB, triangle[remainingIndexInTriag])) {
						return vertexB;
					}
				}

			}
		}

		return -1;
	}

	/**
	 * Returns true if the edge between vertexA and vertexB is an outline edge, and
	 * false otherwise.
	 * */
	bool IsOutlineEdge (int vertexA, int vertexB)
	{
		int sharedTriangleCount = 0;

		foreach (var triangle in trianglesDictionary [vertexA]) {
			if (triangle.Contains (vertexB)) {
				sharedTriangleCount++;
				if (sharedTriangleCount > 1) {
					break;
				}
			}
		}

		return sharedTriangleCount == 1;
	}

	void CalculateMeshOutlines ()
	{
		for (int vertexIndex = 0; vertexIndex < vertices.Count; vertexIndex++) {
			if (!checkedVertices[vertexIndex]) {
				int newOutlineVertex = GetConnectedVertexInDirection (vertexIndex, true);

				if (newOutlineVertex >= 0) {
					Outline newOutline = new Outline (false);
					outlines.Add (newOutline);
					FollowOutline (vertexIndex, outlines.Count - 1);
				}
			}
		}
//		Debug.Log (outlines.Count + " outlines."); 
//		int total = 0;
//		foreach (var outline in outlines) {
//			total += outline.Count;
//		}
//		Debug.Log ("Average len: " + total / (float)outlines.Count); 
	}

	bool IsClockwise (params int[] vertexIndeces)
	{
		float sum = 0;
		for (int i = 0; i < vertexIndeces.Length; i++) {
			Vector2 pos1 = (Vector2)vertices [vertexIndeces [i]];
			Vector2 pos2 = (Vector2)vertices [vertexIndeces [(i + 1) % vertexIndeces.Length]];
			sum += (pos2.x - pos1.x) * (pos2.y + pos1.y);
		}
		return sum > 0f;
	}

	void FollowOutline (int vertexIndex, int outlineIndex, bool goClockwise = true)
	{
		bool underLimit = true;
		// Add the origin point 
		Outline currentOutline = outlines [outlineIndex];

		currentOutline.AddFirst (vertexIndex, vertices [vertexIndex]);

		int nextClockwise = 0, nextCClockwise = 0;

		while ((nextClockwise >= 0 || nextCClockwise >= 0) && underLimit) {
			// Get the next points of the outline in clockwise and counter-clockwise directions
			if (nextClockwise >= 0) {
				nextClockwise = GetConnectedVertexInDirection (currentOutline.Last, true); 
			}
			if (nextCClockwise >= 0) {
				nextCClockwise = GetConnectedVertexInDirection (currentOutline.First, false);
			}

			if (nextClockwise >= 0) {
				// There is a next point clockwise
				checkedVertices [currentOutline.Last] = true;
				currentOutline.AddLast (nextClockwise, vertices [nextClockwise]); 
			}
			if (nextCClockwise >= 0) {
				// There is a next point counter-clockwise
				checkedVertices [currentOutline.First] = true;
				currentOutline.AddFirst (nextCClockwise, vertices [nextCClockwise]);
			}
			underLimit = currentOutline.Count < MAX_OUTLINE_LENGTH;
		}

		if (underLimit) {
			checkedVertices [currentOutline.First] = true;
			checkedVertices [currentOutline.Last] = true;
		}
	}

	/**
	 * Adds a triangle to the triangles dictionary in the list of triangles corresponding the
	 * provided vertex key.
	 * */
	void AddTriangleToDictionary (int vertexIndexKey, Triangle triangle)
	{
		if (!trianglesDictionary.ContainsKey (vertexIndexKey)) {
			trianglesDictionary.Add (vertexIndexKey, new List<Triangle> ());
		}

		trianglesDictionary [vertexIndexKey].Add (triangle); 
	}

	void RemoveTriangleFromDictionary (Triangle t)
	{
		trianglesDictionary [t [0]].Remove (t);
		trianglesDictionary [t [1]].Remove (t);
		trianglesDictionary [t [2]].Remove (t);
	}

	void RemoveTriangleFromTrianglesList (Triangle triangle)
	{
		for (int i = 0; i < triangles.Count - 2; i++) {
			if (triangles[i] == triangle [0] && triangles[i + 1] == triangle [1] && triangles[i + 2] == triangle [2]) {
				for (int tripleCount = 0; tripleCount < 3; tripleCount++) {
					triangles.RemoveAt (i);
				}
				return;
			}
		}
	}

	/**
	 * Deletes all triangles inside of a square, i.e. deletes squares made up of the control nodes and 
	 * midpoints of the square.
	 * */
	void DeleteTrianglesForSquare (Square sq)
	{
		List<Node> nodes = new List<Node> (sq.GetNodes ());
		List<int> nodeVertices = new List<int> ();	// WAS HASH SET
		foreach (var node in nodes) {
			if (node.vertexIndex >= 0) {
				nodeVertices.Add (node.vertexIndex);
			}
		}
		foreach (var node in nodes) {
			if (!trianglesDictionary.ContainsKey (node.vertexIndex)) {
				continue;
			}
			Triangle[] triags = trianglesDictionary [node.vertexIndex].ToArray ();
			foreach (var triangle in triags) {
				// Make sure that all vertices of the triangle belong to this square
				// If so, delete the triangle
				if (nodeVertices.Contains (triangle [0])
					&& nodeVertices.Contains (triangle [1])
					&& nodeVertices.Contains (triangle[2])) {
					RemoveTriangleFromDictionary (triangle);
					RemoveTriangleFromTrianglesList (triangle);
				}
			}
		}
	}
	#endregion

	void Generate2DColliders() 
	{
		// Destroy previous colliders
		for (int i = 0; i < currentColliders.Count; i++) {
			Destroy(currentColliders[i]);
		}
		currentColliders.Clear ();

//		CalculateMeshOutlines ();

		foreach (var outline in outlines) {
			EdgeCollider2D edgeCollider = gameObject.AddComponent<EdgeCollider2D>();
			currentColliders.Add (edgeCollider);

			Vector2[] edgePoints = new Vector2[outline.Count];
			int i = 0;
			foreach (var point in outline.outlineVertices) {
				edgePoints[i] = (Vector2)vertices[point];
				i++;
			}
			edgeCollider.points = edgePoints;
		}
	}

	void Update2DColliders ()
	{
		// Destroy previous colliders

		int colliderIndex = 0;
		foreach (var outline in outlines) {
			EdgeCollider2D edgeCollider = null;
			if (colliderIndex < currentColliders.Count) {
				edgeCollider = currentColliders [colliderIndex];
				colliderIndex++;
			} else {
				// A new collider will have to be made
				edgeCollider = gameObject.AddComponent<EdgeCollider2D>();
				currentColliders.Add (edgeCollider); 
				colliderIndex++;
			}
			// Update the collider's edge points
			Vector2[] edgePoints = new Vector2[outline.Count];
			int i = 0;
			foreach (var point in outline.outlineVertices) {
				edgePoints[i] = (Vector2)vertices[point];
				i++;
			}
			edgeCollider.points = edgePoints;
		}

		while (colliderIndex < currentColliders.Count) {
			// Destroy any extra colliders
			Destroy (currentColliders [colliderIndex]);
			currentColliders.RemoveAt (colliderIndex); 
			colliderIndex++;
		}
	}

	Color GetColor (int wallType)
	{
		switch (wallType) {
		case (int)RandomLevelGenerator.CellFillType.Empty:
			return Color.white;
		case (int)RandomLevelGenerator.CellFillType.Wall:
			return Color.gray;
		case (int)RandomLevelGenerator.CellFillType.SolidWall:
			return Color.black;
		default:
			return Color.red;
		}
	}

}
