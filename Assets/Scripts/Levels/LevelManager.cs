﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class LevelManager : MonoBehaviour
{
	public static LevelManager Instance { get; private set; }

	public RandomLevelGenerator levelGenerator;

	public DrawMap minimap;

	private int width;
	private int height;

	public ProceduralLevel currentLevel;

	void Awake ()
	{
		if (Instance) {
			Debug.LogWarning ("Another instance of LevelManager found in the scene. Destroying this game object...");
			Destroy (gameObject);
		} else {
			Instance = this;
		}
	}

	void Destroy ()
	{
		if (Instance == this) {
			Instance = null;
		}
	}

	public Rect GetCurrentLevelRect ()
	{
		return currentLevel.boundingBox;
	}

	public virtual void SpawnLevel ()
	{
		// Generate a new level using the level generator
		levelGenerator.GenerateLevel ();
		currentLevel = new ProceduralLevel (levelGenerator.levelMap, levelGenerator.width, levelGenerator.height);

		width = currentLevel.width;
		height = currentLevel.height;

		if (minimap) {
			minimap.StartDrawing (); 
		}
	}

//	protected virtual void RetrieveSpawnedLevel ()
//	{
//		// Check if all networked variables have been synced properly and only then build the level
//		if (syncedLevel.Count > 0 && syncedLevel.Count == width * height) {
//			// We have received a valid level. Copy all values in a simple array.
//			int[] tmpArray = new int[syncedLevel.Count];
//			for (int i = 0; i < syncedLevel.Count; i++) {
//				tmpArray [i] = syncedLevel [i];
//			}
//			// Reconstruct the level matrix from the synchronized data
//			currentLevel = new ProceduralLevel (Array2Matrix (tmpArray, width, height), width, height); 
//
//			// Create a level from the level matrix
//			levelGenerator.GenerateFromTemplate (currentLevel.GetLevelMatrix ());
//		}
//	}

//	public override void OnStartServer ()
//	{
//		// Generate a random level
//		SpawnLevel ();
//	}

//	public override void OnStartClient ()
//	{
//		if (!isServer) {
//			RetrieveSpawnedLevel ();		
//		}
//		// Start the minimap drawing, if one is available
//		if (minimap) {
//			minimap.StartDrawing (); 	
//		}
//	}

	/**
	 * Puts all rows of the level matrix sequentially into a 1D array.
	 * */
	public static int[] Matrix2Array (int[,] level)
	{
		int counter = 0;
		int width = level.GetLength (0);
		int height = level.GetLength (1);
		int[] flatArray = new int[width * height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				flatArray [counter] = level [x, y];
				counter++;
			}
		}
		return flatArray;
	}

	/**
	 * Splits the flat array into rows of length width and puts those rows and
	 * columns, into a 2D array which is then returned.
	 * */
	public static int[,] Array2Matrix (int[] array, int width, int height)
	{
		if (array.Length != width * height) {
			throw new UnityException ("Flat array length does not correspond to provided width and height.");
		}
		int[,] matrix = new int[width, height];
		int counter = 0;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				matrix [x, y] = array [counter];
				counter++;
			}
		}
		return matrix;
	}

	/**
	 * Destroys a section of the wall of a level with the specified radius and at the specified position.
	 * This causes modifications to the level walls mesh.
	 * */
	public void RpcExplodeWall (Vector2 worldPosition, int explosionRadius)
	{
		int xOrigin, yOrigin;
		currentLevel.World2Map (worldPosition, out xOrigin, out yOrigin);
		int empty = (int)RandomLevelGenerator.CellFillType.Empty;

		// Sometimes, world2map will not return a tile that has a wall in it since it is possible to hit
		// the wall segment between tiles. In this case, find the nearest tile which is not empty (i.e. has a wall in it)
		if (currentLevel.GetCell (xOrigin, yOrigin) == empty) {
			bool foundNonEmpty = false;
			for (int xOffset = -1; xOffset <= 1 && !foundNonEmpty ; xOffset++) {
				for (int yOffset = -1; yOffset <= 1 && !foundNonEmpty; yOffset++) {
					int x = xOrigin + xOffset;
					int y = yOrigin + yOffset;
					if (currentLevel.InRange (x, y) && currentLevel.GetCell (x, y) != empty) {
						xOrigin += xOffset;
						yOrigin += yOffset;
						foundNonEmpty = true;
					}
				}
			}
		}

		for (int x = xOrigin - explosionRadius; x <= xOrigin + explosionRadius; x++) {
			for (int y = yOrigin - explosionRadius; y <= yOrigin + explosionRadius; y++) {
				if (currentLevel.InRange (x, y)) {
					currentLevel.SetCell (x, y, empty);
				}
			}
		}

		levelGenerator.UpdateFromTemplate (
			currentLevel.GetLevelMatrix (), 
			xOrigin - explosionRadius, 
			yOrigin - explosionRadius, 
			xOrigin + explosionRadius, 
			yOrigin + explosionRadius);

		EffectsManager.Instance.WallExplosionAt ((Vector3)worldPosition + Vector3.back);
	}
}