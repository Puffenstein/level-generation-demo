﻿using UnityEngine;
using System.Collections;

/**
 * Copyright 2016, Ivaylo Parvanov, All rights reserved.
 * This class can hold the layout of a procedurally generated level.
 * */
public class ProceduralLevel
{
	/** This event is invoked whenever this level's level map is changed via the SetCell method of this class. */
	public event System.Action <RandomLevelGenerator.TileCoord> OnLevelChanged;
	public int width { get; protected set; }
	public int height { get; protected set; }
	public Rect boundingBox { get; protected set; }
	private int[,] levelMap;

	public ProceduralLevel (int[,] map, int width, int height)
	{
		if (map == null || map.GetLength (0) != width || map.GetLength (1) != height ) {
			throw new UnityException ("Error creating procedural level object."); 
		}
		this.levelMap = map;
		this.width = width;
		this.height = height;

		Vector2 size = new Vector2 (width, height);
		Vector2 position = -size / 2f;
		boundingBox = new Rect (size, position);
	}

	/**
	 * Converts a map point to world point, but without a z coordinate.
	 * */
	public Vector2 Map2World (int x, int y)
	{
		return new Vector2 (-width / 2f, -height / 2f) + Vector2.right * (x + 0.5f) + Vector2.up * (y + 0.5f);
	}

	/**
	 * Converts a world position to coordinates in the level map.
	 * */
	public void World2Map (Vector2 worldPos, out int x, out int y)
	{
		x = Mathf.FloorToInt (worldPos.x + width / 2f);
		y = Mathf.FloorToInt (worldPos.y + height / 2f);
	}

	/**
	 * Returns the value of the cell (x, y) of the current level map.
	 * There are no range checks done on (x, y).
	 * */
	public int GetCell (int x, int y)
	{
		return levelMap [x, y];
	}

	/**
	 * Sets the value of the cell (x, y) of the current level map, 
	 * and also notifyies all delegates waiting on that change.
	 * There are no range checks on the cell coordinates.
	 * */
	public void SetCell (int x, int y, int value)
	{
		levelMap [x, y] = value;
		if (OnLevelChanged != null) {
			OnLevelChanged (new RandomLevelGenerator.TileCoord (x, y));
		}
	}

	/**
	 * Selects x and y at random such that the map tile with coordinates (x, y) is an empty tile.
	 * */
	public void GetRandomEmptyCell (out int x, out int y)
	{
		do {
			x = Random.Range (0, width);
			y = Random.Range (0, height);
		} while (!IsEmpty (x, y));
	}

	/**
	 * Returns true if tile (x, y) of the level map is empty.
	 * */
	public bool IsEmpty (int x, int y)
	{
		if (InRange (x, y)) {
			return levelMap [x, y] == (int)RandomLevelGenerator.CellFillType.Empty;
		} else {
			return false;
		}
	}

	/**
	 * Returns the value of the tile (x, y). If the tile is out of range, -1 is returned.
	 * */
	public int GetTile (int x, int y)
	{
		if (InRange (x, y)) {
			return levelMap [x, y];
		} else {
			return -1;
		}
	}

	/**
	 * Returns a reference to the current level map.
	 * */
	public int[,] GetLevelMatrix ()
	{
		return levelMap;
	}

	/**
	 * Returns true if the coordinates x and y are in the range of the level map,
	 * and are therefore valid coordinates.
	 * */
	public bool InRange (int x, int y)
	{
		return x >= 0 && x < width && y >= 0 && y < height;
	}
}
