﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

/**
 * Copyright 2016, Ivaylo Parvanov, All rights reserved.
 * */
public class Level : NetworkBehaviour
{
	/** The bounds of this level is the rectangle containing the entire level. */
	public Rect BoundsRect { get; protected set; }

	SyncListBool destroyedObjects = new SyncListBool ();

	Destructible[] destructibleObjects;

	[SerializeField] GameObject levelBounds;

	void Awake ()
	{
		UpdateBounds ();
	}

	/**
	 * Recalculates the level bounds and assings the resulting rect to the Bounds
	 * property
	 * */
	protected void UpdateBounds ()
	{
		Vector2 extents = new Vector2 (levelBounds.transform.localScale.x / 2f, levelBounds.transform.localScale.y / 2f);

		Rect r = new Rect ();
		r.min = (Vector2)transform.position - extents;
		r.max = (Vector2)transform.position + extents;

		BoundsRect = r;
	}

	/**
	 * Returns true if the provided 2D position is located inside the level.
	 * Otherwise, returns false.
	 * */
	public bool IsWithinBounds (Vector2 position)
	{
		return BoundsRect.Contains (position);
	}

	/**
	 * A recursive algorithm for getting all objects that have a script implementing
	 * the Destructible interface.
	 * */
	List<Destructible> GetDestructibleObjects (Transform transform, List<Destructible> l)
	{
		if (transform.gameObject.CompareTag ("Destructible")) {
			Destructible d = transform.gameObject.GetComponent<Destructible> ();
			if (d != null) {
				l.Add (d);
			}	
		}

		foreach (Transform child in transform) {
			GetDestructibleObjects (child, l);
		}
		return l;
	}

	public override void OnStartClient ()
	{
		List<Destructible> l = new List<Destructible> ();
		GetDestructibleObjects (transform, l);
		destructibleObjects = l.ToArray ();

		if (!isServer) {
			// Destroy all objects that are marked as destroyed in the destroyedObjects array
			for (int i = 0; i < destructibleObjects.Length; i++) {
				if (destroyedObjects [i]) {
					destructibleObjects [i].Destroy ();
				}
			}
		} else {
			// Server initialises the destroyed objects list with values
			for (int i = 0; i < destructibleObjects.Length; i++) {
				destroyedObjects.Add (destructibleObjects [i].IsDestroyed ());
			}
		}
	}

	/**
	 * Returns the index for the provided destructible. This index can then be used to destroy this destructible
	 * on all clients via the CmdDestroyDestructible (int destructibleIndex) method.
	 * */
	public int GetIndexForDestructible (Destructible d)
	{
		for (int i = 0; i < destructibleObjects.Length; i++) {
			if (destructibleObjects [i].Equals (d)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Destroys the destructible with the given index on all clients by changing a
	 * synchronized array.
	 * */
	[Command]
	public void CmdDestroyDestructible (int destructibleIndex)
	{
		// This runs only on the server
		destroyedObjects [destructibleIndex] = true;
		RpcDestroyDestructible (destructibleIndex);
	}

	/**
	 * Destroys the destructible with the given index on a clien.
	 * */
	[ClientRpc]
	void RpcDestroyDestructible (int destructibleIndex)
	{
		// This code runs on clients and is called by the server when a destructible is destroyed
		destructibleObjects [destructibleIndex].Destroy ();
	}
}
