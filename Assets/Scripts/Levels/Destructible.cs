﻿using UnityEngine;
using System.Collections;

/**
 * This interface allows level objects to be destroyed.
 * */
public interface Destructible
{
	/**
	 * Method called when a destructible object needs to be destroyed.
	 * */
	void Destroy ();

	/**
	 * Method called when the destructible needs to be reset. So, if it was destroyed,
	 * this method should return the object to its initial state before the destruction
	 * took place.
	 * */
	void Reset ();

	/**
	 * Returns true if this destructible object has already been destroyed. If reset is called
	 * and the object returns to its initial state, this method should also return false.
	 * */
	bool IsDestroyed ();

	/**
	 * Returns true if this destructible is the same object as the parameter.
	 * */
	bool Equals (Destructible d);
}
