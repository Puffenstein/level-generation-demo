﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/**
 * Copyright 2016, Ivaylo Parvanov, All rights reserved.
 * This class can be used to create a randomized cave-like map
 * by using the cellular automata algorithm.
 * */
public class RandomLevelGenerator : MonoBehaviour
{
	/**
	 * During smoothing, if a cell has more than this amount of neighbouring cells filled with walls,
	 * then that cell will also be converted to a wall. If the cell has less than this amount of wall
	 * neighbours, it will be converted to an empty cell.
	 * */
	public const int CONVERT_THRESHOLD = 4;

	public enum CellFillType
	{
		// Empty cell with nothing in it at all
		Empty = 0,
		// A normal wall that can be destroyed
		Wall = 1,
		// A wall that cannot be destroyed
		SolidWall = 2,
		// An indestructible wall used on the outside edge of the map.
		OutsideWall = 3
	};

	public int width;
	public int height;
	public int border = 0;
	/** Regions of walls that contain less tiles than this amount will be deleted during the map refinement phase. */
	public int smallestWalledRegion = 8;
	/** Rooms (empty space regions) containing less tiles than this amount will be deleted during the map refinement phase. */
	public int smallestRoomRegion = 6;
	/** The half of the width of paths connecting different rooms/regions on the map. */
	public int connectionsWidth = 1;
	public int iterationsCount = 7;

	public string seed;
	public bool useRandomSeed;

	[Range (0, 100)]
	public int randomFillPercent;

	/** This map determines whether a cell in the level is filled with a wall or not. */
	public int[,] levelMap { get; set; }

	protected LevelMeshGenerator lmg;

	/**
	 * Generates a randomized level as well as a mesh for the level.
	 * */
	public void GenerateLevel ()
	{
		levelMap = new int[width, height];
		RandomFill ();

		for (int i = 0; i < iterationsCount; i++) {
			SmoothMap ();
		}

		RefineMap (); 

		lmg = GetComponent<LevelMeshGenerator> ();
		lmg.GenerateMesh (levelMap, this);
	}

	/**
	 * Instead of generating the level map, this method takes a pre-determined
	 * level map and only creates the mesh for it.
	 * */
	public void GenerateFromTemplate (int[,] levelMap)
	{
		if (levelMap == null || levelMap.GetLength (0) == 0 || levelMap.GetLength (1) == 0) {
			return;
		}
		this.levelMap = levelMap;
		LevelMeshGenerator lmg = GetComponent<LevelMeshGenerator> ();
		lmg.GenerateMesh (levelMap, this);
	}

	public void UpdateFromTemplate (int[,] levelMap, int fromX, int fromY, int toX, int toY)
	{
		this.levelMap = levelMap;
		lmg.PartialMeshUpdate (levelMap, fromX, fromY, toX, toY);
	}

	public LevelMeshGenerator GetLastUsedMeshGenerator ()
	{
		return lmg;
	}

	/**
	 * Fills the level map randomly with walls. This method takes into
	 * account the current seed, if useRandomSeed is false. It also uses
	 * the randomFillPercent as the probability to spawn a wall.
	 * */
	void RandomFill ()
	{
		if (useRandomSeed) {
			System.DateTime time = System.DateTime.Now;
			seed = time.ToLongTimeString () + ":" + time.Millisecond;
		}

		UnityEngine.Random.InitState (seed.GetHashCode ());

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (x < border || x > width - 1 - border || y < border || y > height - 1 - border) {
					levelMap [x, y] = (int)CellFillType.OutsideWall;
				} else {
					levelMap [x, y] = (UnityEngine.Random.Range (0, 101) < randomFillPercent) ? (int)CellFillType.Wall : (int)CellFillType.Empty;	
				}
			}
		}
	}

	/**
	 * This is the smoothing iteration algorithm that generates a cave-like structure upon running it
	 * repeatedly on a randomly filled level map.
	 * */
	void SmoothMap ()
	{
		// Smooth all cells in the map, except the outer rim of solid walls.
		for (int x = 1; x < width - 1; x++) {
			for (int y = 1; y < height - 1; y++) {
				int surroundWallCount = GetSurroundingWallCount (x, y);

				if (surroundWallCount > CONVERT_THRESHOLD) {
					levelMap [x, y] = (int)CellFillType.Wall;
				} else if (surroundWallCount < CONVERT_THRESHOLD - 1) {
					levelMap [x, y] = (int)CellFillType.Empty;
				}
			}
		}
	}

	/**
	 * Returns the number of walls in the cells neigbouring cell (neighbourX, neighbourY).
	 * */
	int GetSurroundingWallCount (int centerX, int centerY)
	{
		int wallCount = 0;
		for (int neighbourX = centerX - 1; neighbourX <= centerX + 1; neighbourX++) {
			for (int neighbourY = centerY - 1; neighbourY <= centerY + 1; neighbourY++) {
				if (InRange (neighbourX, neighbourY)
				    && (neighbourX != centerX || neighbourY != centerY)
				    && IsWall (neighbourX, neighbourY)) {
					wallCount++;
				}
			}
		}
		return wallCount;
	}

	/**
	 * Method returns true if there is any type of wall in the provided 
	 * cell of the current level map, false otherwise.
	 * */
	public bool IsWall (int x, int y)
	{
		return IsWall (x, y, this.levelMap);
	}

	/**
	 * Method returns true if there is any type of wall in the provided cell of the levelMap, false otherwise.
	 * */
	public static bool IsWall (int x, int y, int[,] levelMap)
	{
		return IsWall (levelMap [x, y]);
	}

	/**
	 * Returns true if the integer value provided corresponds to a wall or a solid wall.
	 * */
	public static bool IsWall (int cellValue)
	{
		return cellValue == (int)CellFillType.Wall
		|| cellValue == (int)CellFillType.SolidWall
		|| cellValue == (int)CellFillType.OutsideWall;
	}

	/**
	 * Convenience method returning true if the x, y coordinates are valid cell 
	 * coordinates, or false if (x, y) is out of range.
	 * */
	bool InRange (int x, int y)
	{
		return x > 0 && x < width - 1 && y > 0 && y < height - 1;
	}

	/**
	 * Uses regions detection to refine the map by removing walled regions that are too small, and
	 * by connecting empty regions that are disconnected.
	 * */
	void RefineMap ()
	{
		// 1. Small walls deletion
		{
			List<List<TileCoord>> wallRegions = GetRegions ((int)CellFillType.Wall);
			foreach (var region in wallRegions) {
				if (region.Count < smallestWalledRegion) {
					// Delete this walled region as it is too small.
					foreach (var tile in region) {
						levelMap [tile.tileX, tile.tileY] = (int)CellFillType.Empty;
					}
				}
			}
		}

		// 2. Small rooms deletion
		List<Room> disconnectedRooms = new List<Room> ();
		{
			List<List<TileCoord>> roomRegions = GetRegions ((int)CellFillType.Empty);
			foreach (var room in roomRegions) {
				if (room.Count < smallestRoomRegion) {
					// Delete this room as it is too small
					foreach (var tile in room) {
						levelMap [tile.tileX, tile.tileY] = (int)CellFillType.Wall;
					}
				} else {
					// This room is too big to remove. Add it to the disconnectedRooms list
					disconnectedRooms.Add (new Room (room, levelMap));
				}
			}
		}

		// 3. Room connections
		if (disconnectedRooms.Count > 0) {
			disconnectedRooms.Sort ();
			disconnectedRooms [0].isMainRoom = true;
			disconnectedRooms [0].isAccessibleFromMainRoom = true;
			ConnectClosestRooms (disconnectedRooms);
		}
			
	}

	void ConnectClosestRooms (List<Room> disconnectedRooms, bool forceAccessibilityFromMainRoom = false)
	{
		if (disconnectedRooms.Count < 2) {
			return;
		}
		List<Room> roomListA = new List<Room> ();
		List<Room> roomListB = new List<Room> ();

		if (forceAccessibilityFromMainRoom) {
			foreach (var room in disconnectedRooms) {
				if (room.isAccessibleFromMainRoom) {
					roomListB.Add (room); 
				} else {
					roomListA.Add (room); 
				}
			}
		} else {
			roomListA = disconnectedRooms;
			roomListB = disconnectedRooms;
		}

		TileCoord bestTileInA = new TileCoord ();
		TileCoord bestTileInB = new TileCoord ();
		Room bestRoomA = null;
		Room bestRoomB = null;
		int bestDistance = int.MaxValue;
		bool connectionFound = false;

		foreach (var roomA in roomListA) {
			if (!forceAccessibilityFromMainRoom) {
				connectionFound = false;	// Reset the connection found for every room on first pass
				if (roomA.connectedRooms.Count > 0) {
					continue;
				}
			}

			foreach (var roomB in roomListB) {
				if (roomA == roomB || roomA.IsConnected (roomB)) {
					continue;
				}

				int distance;
				TileCoord tileInCurrentRoom;
				TileCoord tileInOtherRoom;
				distance = CalculateConnection (roomA, roomB, out tileInCurrentRoom, out tileInOtherRoom);
				if (distance < bestDistance) {
					// This is the new closest room to the current one
					bestRoomA = roomA;
					bestRoomB = roomB;
					bestTileInA = tileInCurrentRoom;
					bestTileInB = tileInOtherRoom;
					bestDistance = distance;
					connectionFound = true;
				}
			}

			if (connectionFound && !forceAccessibilityFromMainRoom) {
				// On first pass, connect pairs of rooms immediately
				ConnectPairOfRooms (bestRoomA, bestRoomB, bestTileInA, bestTileInB); 	
			}
		}

		if (connectionFound && forceAccessibilityFromMainRoom) {
			// On second pass, connect pairs of rooms at the end
			ConnectPairOfRooms (bestRoomA, bestRoomB, bestTileInA, bestTileInB);
			ConnectClosestRooms (disconnectedRooms, true); 
		}

		if (!forceAccessibilityFromMainRoom) {
			ConnectClosestRooms (disconnectedRooms, true); 
		}
	}

	void ConnectPairOfRooms (Room roomA, Room roomB, TileCoord tileInRoomA, TileCoord tileInRoomB)
	{
		// Mark the two rooms as connected
		Room.ConnectRooms (roomA, roomB);
		foreach (var tile in GetLine (tileInRoomA, tileInRoomB)) {
			EmptyCircle (tile, connectionsWidth);
		}
	}

	/**
	 * Returns the tiles laying on the line between tileA and tileB.
	 * */
	List<TileCoord> GetLine (TileCoord tileA, TileCoord tileB)
	{
		List<TileCoord> tilesOnLine = new List<TileCoord> ();

		// Find the equation of the segment between tileA and tileB
		int dx = tileB.tileX - tileA.tileX;
		int dy = tileB.tileY - tileA.tileY;
		// Use a known point to calculate n
		float n = (dx != 0) ? (tileA.tileY - tileA.tileX * (float)dy / (float)dx) : 0;

		if (dx * dx >= dy * dy) {	// Compare the absolute values of dx and dy. Squaring the values is an easy way to make them positive
			// We will be stepping on x
			int step = Math.Sign (dx);
			float dydx = (float)dy / dx;
			for (int x = tileA.tileX; x != tileB.tileX; x += step) {
				float y = dydx * x + n;
				int yCoord = Mathf.FloorToInt (y + 0.5f); 
				tilesOnLine.Add (new TileCoord (x, yCoord));
			}
		} else {
			// We will be stepping on y
			int step = Math.Sign (dy);
			float dxdy = (float)dx / dy;
			for (int y = tileA.tileY; y != tileB.tileY; y += step) {
				float x;
				if (dx == 0) {
					x = tileA.tileX;
				} else {
					x = (y - n) * dxdy;
				}
				int xCoord = Mathf.FloorToInt (x + 0.5f);
				tilesOnLine.Add (new TileCoord (xCoord, y)); 
			}
		}
		return tilesOnLine;
	}

	/**
	 * Creates a circle of empty tiles on the level map centred on the provided tile and
	 * with the specified radius.
	 * */
	void EmptyCircle (TileCoord tile, int radius)
	{
		int empty = (int)CellFillType.Empty;
		for (int x = -radius; x <= radius; x++) {
			for (int y = -radius; y <= radius; y++) {
				if (x*x + y*y <= radius*radius) {
					int drawX = tile.tileX + x;
					int drawY = tile.tileY + y;
					if (InRange(drawX, drawY)) {
						levelMap[drawX,drawY] = empty;
					}
				}
			}
		}
	}

	/**
	 * Calculates and returns the shortest squared distance between rooms a and b.
	 * Also, the values of aTile and bTile are set to the closest tiles between the two rooms.
	 * */
	int CalculateConnection (Room a, Room b, out TileCoord aTile, out TileCoord bTile)
	{
		// Make sure a is the smaller room
		if (a.roomSize > b.roomSize) {
			Room tmp = a;
			a = b;
			b = tmp;
		}
		// Make a map where every point is either 1 for tiles belonging to room a, 2 for tiles belonging to room b, or 0.
		int[,] map = new int[width, height];
		foreach (var tile in a.tiles) {
			map [tile.tileX, tile.tileY] = 1;
		}
		foreach (var tile in b.tiles) {
			map [tile.tileX, tile.tileY] = 2;
		}

		aTile = new TileCoord ();
		bool aTileSet = false;
		bTile = new TileCoord ();
		bool bTileSet = false;

		Queue<TileCoord> tilesQueue = new Queue<TileCoord> ();
		bool[,] coordinateDiscovered = new bool[width, height];
		tilesQueue.Enqueue (new TileCoord ((int)a.boundingRect.center.x, (int)a.boundingRect.center.y));
		coordinateDiscovered [(int)a.boundingRect.center.x, (int)a.boundingRect.center.y] = true;

		while (tilesQueue.Count > 0) {
			TileCoord tile = tilesQueue.Dequeue (); 
			if (bTileSet) {
				break;
			}
			if (!bTileSet && map[tile.tileX, tile.tileY] == 2) {
				bTile = tile;
				bTileSet = true;
			}
			// Enqueue neighbours
			for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++) {
				for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++) {
					if (InRange (x, y) && !coordinateDiscovered [x, y] && (x == tile.tileX || y == tile.tileY)) {
						tilesQueue.Enqueue (new TileCoord (x, y));
						coordinateDiscovered [x, y] = true;
					}
				}
			}
		}

		tilesQueue.Clear ();
		coordinateDiscovered = new bool[width, height];
		tilesQueue.Enqueue (new TileCoord (bTile.tileX, bTile.tileY));
		coordinateDiscovered [bTile.tileX, bTile.tileY] = true;

		while (tilesQueue.Count > 0) {
			TileCoord tile = tilesQueue.Dequeue ();
			if (aTileSet) {
				break;
			}
			if (!aTileSet && map[tile.tileX, tile.tileY] == 1) {
				aTile = tile;
				aTileSet = true;
			}
			// Enqueue neighbours
			for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++) {
				for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++) {
					if (InRange (x, y) && !coordinateDiscovered [x, y] && (x == tile.tileX || y == tile.tileY)) {
						tilesQueue.Enqueue (new TileCoord (x, y));
						coordinateDiscovered [x, y] = true;
					}
				}
			}
		}

		if (!aTileSet || !bTileSet) {
			throw new UnityException ("Calculating connection between rooms failed.");
		}
		int dx = bTile.tileX - aTile.tileX;
		int dy = bTile.tileY - aTile.tileY;
		return dx * dx + dy * dy;
	}

	#region Region detection

	/**
	 * Returns all tiles of the region starting at tile (startX, startY).
	 * If, for example, the starting tile is empty space, all tiles connected to
	 * the starting tiles that are empty spaces will be returned in the list, as well
	 * as the starting tile itself.
	 * */
	public List<TileCoord> GetRegion (int startX, int startY)
	{
		List<TileCoord> region = new List<TileCoord> ();
		bool [,] checkedTiles = new bool[width, height];
		// Determine the tile type for the region based on the start tile
		int tileType = levelMap [startX, startY];
		// Queue used for flood fill
		Queue<TileCoord> queue = new Queue<TileCoord> ();

		queue.Enqueue (new TileCoord (startX, startY)); 
		checkedTiles [startX, startY] = true;

		while (queue.Count > 0) {
			TileCoord tile = queue.Dequeue ();
			region.Add (tile); 

			for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++) {
				for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++) {
					if (InRange (x, y) && ((tile.tileX == x && tile.tileY != y) || (tile.tileY == y && tile.tileX != x))) {
						// This tile is not out of range, not the current tile, and it is directly above, below, left or right of the tile
						if (!checkedTiles[x, y] && levelMap[x, y] == tileType) {
							// This is a tile of the correct type that has not been added to the queue yet
							queue.Enqueue (new TileCoord (x, y));
							checkedTiles [x, y] = true;
						}
					}
				}
			}
		}
		return region;
	}

	/**
	 * Returns all regions on the map of tiles of the specified type.
	 * */
	public List<List<TileCoord>> GetRegions (int tileType)
	{
		List<List<TileCoord>> regions = new List<List<TileCoord>> ();
		bool[,] checkedTiles = new bool[width, height];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (!checkedTiles[x, y] && levelMap[x, y] == tileType) {
					// This is a tile of the correct type that has not yet been checked.
					// Start a new region flood fill from that tile
					regions.Add (GetRegion (x, y));
					// Do not forget to mark all tiles of this new region as checked
					foreach (var tile in regions[regions.Count - 1]) {
						checkedTiles [tile.tileX, tile.tileY] = true;
					}
				}
			}
		}
		return regions;
	}

	#endregion

	public Vector3 Coord2WorldPoint (TileCoord coord) 
	{
		return new Vector3 (-width / 2 + .5f + coord.tileX, -height / 2 + .5f + coord.tileY, -3);
	}

	// TODO DELETE DEBUG
//	void Update ()
//	{
//		if (Input.GetKeyDown (KeyCode.R)) {
//			GenerateLevel ();
//		}		
//	}

	#region Helper classes and structs

	public struct TileCoord
	{
		public int tileX;
		public int tileY;

		public TileCoord (int x, int y)
		{
			this.tileX = x;
			this.tileY = y;
		}
	}

	public class Room : IComparable<Room>
	{
		public List<TileCoord> tiles;
		public List<Room> connectedRooms;
		public Rect boundingRect;
		public bool isAccessibleFromMainRoom;
		public bool isMainRoom;

		public int roomSize {
			get {
				if (tiles != null) {
					return tiles.Count;
				}
				return 0;
			}
		}

		public Room ()
		{
			tiles = new List<TileCoord> ();
			connectedRooms = new List<Room> ();
			boundingRect = new Rect ();
		}

		public Room (List<TileCoord> roomTiles, int[,] levelMap)
		{
			tiles = roomTiles;
			connectedRooms = new List<Room> ();
			// Calculate the bounding rect
			int minX = int.MaxValue, minY = int.MaxValue, maxX = int.MinValue, maxY = int.MinValue;
			foreach (var tile in tiles) {
				minX = Mathf.Min (minX, tile.tileX);
				minY = Mathf.Min (minY, tile.tileY);
				maxX = Mathf.Max (maxX, tile.tileX); 
				maxY = Mathf.Max (maxY, tile.tileY); 
			}
			boundingRect = new Rect ();
			boundingRect.xMin = minX;
			boundingRect.xMax = maxX;
			boundingRect.yMin = minY;
			boundingRect.yMax = maxY;
		}

		public void SetAccessibleFromMainRoom ()
		{
			if (!isAccessibleFromMainRoom) {
				isAccessibleFromMainRoom = true;
				foreach (var room in connectedRooms) {
					room.SetAccessibleFromMainRoom (); 
				}
			}
		}

		public static void ConnectRooms (Room a, Room b)
		{
			if (a.isAccessibleFromMainRoom) {
				b.SetAccessibleFromMainRoom (); 
			} else if (b.isAccessibleFromMainRoom) {
				a.SetAccessibleFromMainRoom (); 
			}
			a.connectedRooms.Add (b); 
			b.connectedRooms.Add (a); 
		}

		public bool IsConnected (Room otherRoom)
		{
			return connectedRooms.Contains (otherRoom);
		}

		#region IComparable implementation

		int IComparable<Room>.CompareTo (Room other)
		{
			return other.roomSize.CompareTo (roomSize);
		}

		#endregion
	}

	#endregion
}
