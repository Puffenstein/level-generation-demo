﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	public static CameraController Instance { private set; get; }

	/** This is the tank that the camera is following. */
	Tank tank;

	/**
	 * Sets the tank that the camera will track.
	 * */
	public void SetTank (Tank t)
	{
		tank = t;
	}

	void Awake ()
	{
		if (Instance) {
			Destroy (gameObject);
			Debug.LogWarning ("Another camera with controller already found. Destroying this camera...");
		} else {
			Instance = this;
		}
	}

	void Destroy ()
	{
		if (Instance == this) {
			Instance = null;
		}
	}

	void FixedUpdate ()
	{
		if (tank) {
			// Set the camera to be on top of the tank. Preserve the camera's height.
			Vector3 newPos = tank.transform.position;
			newPos.z = transform.position.z;

			transform.position = newPos;	
		}
	}
}
