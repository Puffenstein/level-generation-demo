﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInfoPanel : MonoBehaviour
{
	[SerializeField] Text nameText;
	[SerializeField] Text scoreText;

	public void SetName (string name)
	{
		nameText.text = name;
	}

	public void SetScore (int score)
	{
		scoreText.text = score.ToString ();
	}
}
