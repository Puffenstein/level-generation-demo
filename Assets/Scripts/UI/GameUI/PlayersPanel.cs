﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlayersPanel : MonoBehaviour
{

	[SerializeField] PlayerInfoPanel playerInfoPanelPrefab;

	Dictionary<string, PlayerInfoPanel> panels = new Dictionary<string, PlayerInfoPanel> ();

	void Start ()
	{
		ConnectedPlayers.OnPlayerAdded += ConnectedPlayers_OnPlayerAdded;
		ConnectedPlayers.OnPlayerRemoved += ConnectedPlayers_OnPlayerRemoved;
	}

	void OnDestroy ()
	{
		ConnectedPlayers.OnPlayerAdded -= ConnectedPlayers_OnPlayerAdded;
		ConnectedPlayers.OnPlayerRemoved -= ConnectedPlayers_OnPlayerRemoved;
	}

	void ConnectedPlayers_OnPlayerRemoved (PlayerID player)
	{
		RemovePlayer (player.playerName);
	}

	void ConnectedPlayers_OnPlayerAdded (PlayerID newPlayer)
	{
		AddPlayer (newPlayer.playerName, 0);
	}

	/**
	 * Creates a new player info panel for the player with the given
	 * name and score and displays it in the players panel.
	 * */
	public void AddPlayer (string name, int score)
	{
		PlayerInfoPanel p = Instantiate (playerInfoPanelPrefab);
		p.transform.SetParent (this.transform, false);

		p.SetName (name);
		p.SetScore (score);

		panels.Add (name, p);
	}

	/**
	 * Destroys the player info panel for the player with the given
	 * name, and removes it from this players panel.
	 * */
	public void RemovePlayer (string name)
	{
		if (panels.ContainsKey (name)) {
			PlayerInfoPanel p = panels [name];
			panels.Remove (name);
			Destroy (p.gameObject);
		}
	}

	/**
	 * Updates the score of an existing player info panel.
	 * If a player info panel for the player with the provided name
	 * is not found, this method does nothing.
	 * */
	public void SetScore (string name, int newScore)
	{
		if (panels.ContainsKey (name)) {
			PlayerInfoPanel p = panels [name];	
			p.SetScore (newScore);
		}
	}
}
