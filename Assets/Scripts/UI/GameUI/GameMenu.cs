﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
	[SerializeField] Text infoText;
	[SerializeField] PlayersPanel PlayersPanel;

	public void Show ()
	{
		infoText.gameObject.SetActive (false);
		gameObject.SetActive (true);
	}

	public void Hide ()
	{
		gameObject.SetActive (false);
	}
}
