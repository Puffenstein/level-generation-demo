﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
	public static GameController Instance;

	public Tank localPlayerTank;

	void Awake ()
	{
		if (Instance) {
			Destroy (gameObject);
		} else {
			#if UNITY_ANDROID
			Application.targetFrameRate = 60;
			#endif
			Instance = this;
		}
	}

	void Start ()
	{
		RegenerateLevel ();
	}

	void OnDestroy ()
	{
		if (Instance == this) {
			Instance = null;
		}
	}

//	public void SetTank (Tank tank)
//	{
//		this.localPlayerTank = tank;
//	}

	/**
	 * Replaces the current level with a newly generated one.
	 * */
	public void RegenerateLevel ()
	{
		LevelManager.Instance.SpawnLevel (); 
		localPlayerTank.ResetTank ();
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		} else if (Input.GetKeyDown (KeyCode.S))  {
			RegenerateLevel ();
		}
	}
}