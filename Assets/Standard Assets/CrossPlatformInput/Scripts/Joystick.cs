using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityStandardAssets.CrossPlatformInput
{
	public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
	{
		public enum AxisOption
		{
			// Options for which axes to use
			Both,
			// Use both
			OnlyHorizontal,
			// Only horizontal
			OnlyVertical
			// Only vertical
		}

		Canvas parentCanvas;
		bool isControlling = false;

		public int MovementRange = 240;
		public int ReferenceScreenHeight = 1440;
		public AxisOption axesToUse = AxisOption.Both;
		// The options for the axes that the still will use
		public string horizontalAxisName = "Horizontal";
		// The name given to the horizontal axis for the cross platform input
		public string verticalAxisName = "Vertical";
		// The name given to the vertical axis for the cross platform input

		Vector3 m_StartPos;
		bool m_UseX;
		// Toggle for using the x axis
		bool m_UseY;
		// Toggle for using the Y axis
		CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis;
		// Reference to the joystick in the cross platform input
		CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis;
		// Reference to the joystick in the cross platform input

		void OnEnable ()
		{
			CreateVirtualAxes ();
			Transform t = transform.parent;
			do {
				parentCanvas = t.GetComponent<Canvas> ();
				t = t.parent;
			} while (parentCanvas == null && t != null);
		}

		void Start ()
		{
			m_StartPos = transform.position;
			MovementRange = Mathf.CeilToInt ((float)Screen.height / ReferenceScreenHeight * MovementRange);
		}

		void UpdateVirtualAxes (Vector3 value)
		{
			var delta = m_StartPos - value;
			delta.y = -delta.y;
			delta /= MovementRange;
			if (m_UseX) {
				m_HorizontalVirtualAxis.Update (-delta.x);
			}

			if (m_UseY) {
				m_VerticalVirtualAxis.Update (delta.y);
			}
		}

		void CreateVirtualAxes ()
		{
			// set axes to use
			m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
			m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

			// create new axes based on axes to use
			if (m_UseX) {
				m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis (horizontalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis (m_HorizontalVirtualAxis);
			}
			if (m_UseY) {
				m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis (verticalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis (m_VerticalVirtualAxis);
			}
		}


		public void OnDrag (PointerEventData data)
		{
			Vector3 newPos = Vector3.zero;

			if (m_UseX) {
				int delta = (int)(data.position.x - m_StartPos.x);
//				delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
				newPos.x = delta;
			}

			if (m_UseY) {
				int delta = (int)(data.position.y - m_StartPos.y);
//				delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
				newPos.y = delta;
			}

			Vector3 dragVector = new Vector3 (newPos.x, newPos.y, newPos.z);
			dragVector = Vector3.ClampMagnitude (dragVector, MovementRange);
			dragVector += m_StartPos;

			transform.position = dragVector;
			UpdateVirtualAxes (transform.position);
		}


		public void OnPointerUp (PointerEventData data)
		{
			transform.position = m_StartPos;
			UpdateVirtualAxes (m_StartPos);
		}


		public void OnPointerDown (PointerEventData data)
		{
//			m_StartPos = data.position;
		}

		void Update ()
		{
			if (Input.GetMouseButtonDown (0) && !EventSystem.current.IsPointerOverGameObject ()) {
				m_StartPos = Input.mousePosition;
				RectTransform t = transform as RectTransform;
				t.anchorMin = Vector2.zero;
				t.anchorMax = Vector2.zero;
				t.anchoredPosition = m_StartPos / parentCanvas.scaleFactor;
				isControlling = true;
			} else if (isControlling && Input.GetMouseButton (0)) {
				PointerEventData p = new PointerEventData (EventSystem.current);
				p.position = Input.mousePosition;
				OnDrag (p);
			} else if (isControlling && Input.GetMouseButtonUp (0)) {
				PointerEventData p = new PointerEventData (EventSystem.current);
				p.position = Input.mousePosition;
				OnPointerUp (p);
				isControlling = false;
			}
		}

		void OnDisable ()
		{
			// remove the joysticks from the cross platform input
			if (m_UseX) {
				m_HorizontalVirtualAxis.Remove ();
			}
			if (m_UseY) {
				m_VerticalVirtualAxis.Remove ();
			}
		}
	}
}